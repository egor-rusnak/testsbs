﻿using AutoMapper;
using TestsBs.Common.DTOs;
using TestsBs.DAL.Entities;

namespace TestsBs.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
        }
    }
}
