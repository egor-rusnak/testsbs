﻿using AutoMapper;
using TestsBs.Common.DTOs;
using TestsBs.DAL.Entities;


namespace TestsBs.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, Task>();
        }
    }
}
