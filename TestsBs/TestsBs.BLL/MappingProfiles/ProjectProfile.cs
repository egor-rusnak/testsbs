﻿using AutoMapper;
using TestsBs.Common.DTOs.Project;
using TestsBs.DAL.Entities;

namespace TestsBs.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDto, Project>();
        }
    }
}
