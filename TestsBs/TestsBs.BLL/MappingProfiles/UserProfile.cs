﻿using AutoMapper;
using TestsBs.Common.DTOs.User;
using TestsBs.DAL.Entities;

namespace TestsBs.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
        }
    }
}
