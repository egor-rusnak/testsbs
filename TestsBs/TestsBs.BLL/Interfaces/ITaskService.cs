﻿using System.Collections.Generic;
using TestsBs.Common.DTOs;

namespace TestsBs.BLL.Interfaces
{
    public interface ITaskService
    {
        TaskDto AddTask(TaskDto task);
        IEnumerable<TaskDto> GetAllTasks();
        TaskDto GetById(int id);
        void RemoveTask(int id);
        TaskDto UpdateTask(TaskDto task);
        void SetTaskAsFinished(int taskId);
        IEnumerable<TaskDto> GetUnfinishedTasksByUser(int userId);
    }
}