﻿using System.Collections.Generic;
using TestsBs.Common.DTOs;

namespace TestsBs.BLL.Interfaces
{
    public interface ITeamService
    {
        TeamDto AddTeam(TeamDto team);
        IEnumerable<TeamDto> GetAllTeams();
        TeamDto GetById(int id);
        void RemoveTeam(int id);
        TeamDto UpdateTeam(TeamDto team);
        void AddUserToTeam(int teamId, int userId);
    }
}