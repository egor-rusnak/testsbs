﻿using System.Collections.Generic;
using TestsBs.Common.DTOs;
using TestsBs.Common.DTOs.Project;
using TestsBs.Common.DTOs.User;

namespace TestsBs.BLL.Interfaces
{
    public interface IQueryService
    {
        IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>> ExecuteQueryFive();
        IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>> ExecuteQueryFour();
        Dictionary<ProjectDto, int> ExecuteQueryOne(int userId);
        IEnumerable<ProjectInfoDto> ExecuteQuerySeven();
        UserInfoDto ExecuteQuerySix(int userId);
        IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId);
        IEnumerable<TaskDto> ExecuteQueryTwo(int userId);
    }
}