﻿using System.Collections.Generic;
using TestsBs.Common.DTOs.User;

namespace TestsBs.BLL.Interfaces
{
    public interface IUserService
    {
        UserDto AddUser(UserDto user);
        IEnumerable<UserDto> GetAllUsers();
        UserDto GetById(int id);
        void RemoveUser(int id);
        UserDto UpdateUser(UserDto user);
    }
}