﻿using System.Collections.Generic;
using TestsBs.Common.DTOs.Project;

namespace TestsBs.BLL.Interfaces
{
    public interface IProjectService
    {
        ProjectDto AddProject(ProjectDto newProject);
        IEnumerable<ProjectDto> GetAllProjects();
        ProjectDto GetById(int id);
        void RemoveProject(int id);
        ProjectDto UpdateProject(ProjectDto project);
    }
}