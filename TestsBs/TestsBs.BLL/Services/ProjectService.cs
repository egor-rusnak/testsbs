﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services.Abstraction;
using TestsBs.Common.DTOs.Project;
using TestsBs.DAL.Context;
using TestsBs.DAL.Entities;

namespace TestsBs.BLL.Services
{
    public class ProjectService : BaseSerivce, IProjectService
    {
        public ProjectService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }


        public ProjectDto AddProject(ProjectDto newProject)
        {
            var projectEntity = _mapper.Map<Project>(newProject);

            _context.Projects.Add(projectEntity);

            _context.SaveChanges();
            var result = GetProject(projectEntity.Id);
            return _mapper.Map<ProjectDto>(result);
        }

        public void RemoveProject(int id)
        {
            var projectEntity = _context.Projects.FirstOrDefault(p => p.Id == id); //not using getproject because of includes

            if (projectEntity == null) throw new ArgumentException("No entity with this id");

            _context.Remove(projectEntity);
            _context.SaveChanges();
        }

        public ProjectDto UpdateProject(ProjectDto project)
        {
            var projectEntity = _mapper.Map<Project>(project);

            _context.Projects.Update(projectEntity);

            _context.SaveChanges();

            return _mapper.Map<ProjectDto>(GetProject(projectEntity.Id));
        }

        public IEnumerable<ProjectDto> GetAllProjects()
        {
            var elems = GetProjects().ToList();
            return _mapper.Map<IEnumerable<ProjectDto>>(elems);
        }

        public ProjectDto GetById(int id)
        {
            var projectEntity = GetProject(id);

            if (projectEntity == null) throw new ArgumentException("No entity with this id");

            return _mapper.Map<ProjectDto>(projectEntity);
        }

        private IQueryable<Project> GetProjects()
        {
            var projects = _context.Projects.Include(t => t.Team).Include(p => p.Author).Include(p => p.Tasks).ThenInclude(t => t.Performer).ToList();

            //Don't know how to fix... I just don't understand why
            if (projects.Count() == 0 || projects.Count() < _context.Projects.Count())
                return _context.Projects.AsQueryable();
            else
                return projects.AsQueryable();
        }

        private Project GetProject(int id)
        {
            var elems = GetProjects();
            return elems.FirstOrDefault(p => p.Id == id);
        }
    }
}
