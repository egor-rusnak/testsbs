﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services.Abstraction;
using TestsBs.Common.DTOs;
using TestsBs.DAL.Context;
using TestsBs.DAL.Entities;

namespace TestsBs.BLL.Services
{
    public class TeamService : BaseSerivce, ITeamService
    {
        public TeamService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public TeamDto AddTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);

            _context.Teams.Add(teamEntity);

            _context.SaveChanges();
            return _mapper.Map<TeamDto>(GetTeam(teamEntity.Id));
        }

        public TeamDto UpdateTeam(TeamDto team)
        {
            var teamEntity = _mapper.Map<Team>(team);

            _context.Teams.Update(teamEntity);

            _context.SaveChanges();
            return _mapper.Map<TeamDto>(GetTeam(teamEntity.Id));
        }

        public void RemoveTeam(int id)
        {
            var teamEntity = GetTeam(id);

            if (teamEntity == null) throw new ArgumentException("No entity with this id!");

            _context.Teams.Remove(teamEntity);

            _context.SaveChanges();
        }

        public IEnumerable<TeamDto> GetAllTeams()
        {
            return _mapper.Map<IEnumerable<TeamDto>>(GetTeams());
        }

        public TeamDto GetById(int id)
        {
            var teamEntity = GetTeam(id);

            if (teamEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<TeamDto>(teamEntity);
        }

        private IQueryable<Team> GetTeams()
        {
            return _context.Teams.Include(t => t.Members);
        }

        private Team GetTeam(int id)
        {
            return GetTeams().FirstOrDefault(t => t.Id == id);
        }

        public void AddUserToTeam(int teamId, int userId)
        {
            var team = _context.Teams.FirstOrDefault(t => t.Id == teamId);
            var user = _context.Users.Include(u => u.Team).FirstOrDefault(u => u.Id == userId);

            if (team == null)
                throw new ArgumentException("No team with this id");
            if (user == null)
                throw new ArgumentException("No user with this id");
            if (user.Team != null)
                throw new ArgumentException("User already has a team!");

            user.Team = team;
            _context.Update(user);
            _context.SaveChanges();
        }
    }
}
