﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services.Abstraction;
using TestsBs.Common.DTOs;
using TestsBs.Common.DTOs.Project;
using TestsBs.Common.DTOs.User;
using TestsBs.DAL.Context;
using TestsBs.DAL.Entities;
using TestsBs.DAL.Entities.Comparers;

namespace TestsBs.BLL.Services
{
    public class QueryService : BaseSerivce, IQueryService
    {

        private IEnumerable<Project> _projects;

        public QueryService(IMapper mapper, ProjectDbContext context)
            : base(context, mapper)
        {
            _projects = GetProjects();
        }

        public Dictionary<ProjectDto, int> ExecuteQueryOne(int userId)
        {
            _projects = GetProjects();
            return new Dictionary<ProjectDto, int>(_projects
                .SelectMany(p => p.Tasks ?? new List<Task>())
                .Where(t => t.Performer.Id == userId)
                .GroupBy(t => t.ProjectId)
                .Join(_projects, groupTasks => groupTasks.Key, p => p.Id, (groupTasks, p) =>
                      new KeyValuePair<ProjectDto, int>(_mapper.Map<ProjectDto>(p), groupTasks.Count()))) ?? new Dictionary<ProjectDto, int>();
        }

        public IEnumerable<TaskDto> ExecuteQueryTwo(int userId)
        {
            _projects = GetProjects();
            var result = _projects.SelectMany(p => p.Tasks?.Where(t => t.Performer.Id == userId && t.Name.Length < 45));
            return _mapper.Map<IEnumerable<TaskDto>>(result.ToList() ?? new List<Task>());
        }

        // In key - Id, value - name
        public IEnumerable<KeyValuePair<int, string>> ExecuteQueryThree(int userId)
        {
            _projects = GetProjects();
            return _projects
                .SelectMany(p => p.Tasks
                    .Where(t =>
                        t.Performer.Id == userId
                        && t.FinishedAt.HasValue
                        && t.FinishedAt.Value.Year == DateTime.Now.Year
                    ).Select(t => new KeyValuePair<int, string>(t.Id, t.Name))
                ).ToList();
        }

        //In key - Id, value - name, User - UsersList
        public IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>> ExecuteQueryFour()
        {
            _projects = GetProjects();
            return _projects.Select(p => p.Team).Distinct(new TeamsEqualityComparer())
                .Join(
                    _projects.Where(p => p.Author != null).Select(p => p.Author)
                        .Union(_projects.SelectMany(p => p.Tasks.Select(t => t.Performer)))
                        .Distinct(new UsersEqualityComparer())
                        .OrderByDescending(u => u.RegisteredAt),
                    t => t.Id, u => u.TeamId, (t, u) => new { Team = t, User = u }
                )
                .GroupBy(t => new KeyValuePair<int, string>(t.Team.Id, t.Team.Name), e => _mapper.Map<UserDto>(e.User))
                .Where(t => !t.Any(u => u.BirthDay.Year >= DateTime.Now.Year - 10))
                .Select(t => new KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>(new KeyValuePair<int, string>(t.Key.Key, t.Key.Value), t.Select(m => m).ToList()))
                .ToList();
        }

        public IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>> ExecuteQueryFive()
        {
            _projects = GetProjects();
            return _projects.SelectMany(p => p.Tasks.Select(t => t.Performer)
                .Union(_projects.Where(p => p.Author != null).Select(m => m.Author)))
                .Distinct(new UsersEqualityComparer())
                .GroupJoin(
                    _projects.SelectMany(p => p.Tasks),
                        u => u.Id,
                        t => t.Performer.Id,
                        (u, t) => new KeyValuePair<UserDto, IEnumerable<TaskDto>>(_mapper.Map<UserDto>(u), _mapper.Map<IEnumerable<TaskDto>>(t.OrderByDescending(tsk => tsk.Name.Length))))
                .OrderBy(u => u.Key.FirstName)
                .ToList();
        }

        public UserInfoDto ExecuteQuerySix(int userId)
        {
            _projects = GetProjects();
            return _projects.SelectMany(p => p.Tasks.Select(t => t.Performer))
                .Union(_projects.Select(p => p.Author))
                .Distinct(new UsersEqualityComparer())
                .GroupJoin
                (
                    _projects,
                    u => u.TeamId,
                    p => p.Team.Id,
                    (u, p) =>
                      new { User = u, LastProject = p.OrderByDescending(e => e.CreatedAt).First() }
                )
                .GroupJoin(
                    _projects.SelectMany(p => p.Tasks),
                    u => u.User.Id,
                    t => t.Performer.Id,
                    (u, t) => new { u.User, u.LastProject, Tasks = t }
                )
                .Select(e => new UserInfoDto(
                    _mapper.Map<UserDto>(e.User),
                    _mapper.Map<ProjectDto>(e.LastProject),
                    e.LastProject.Tasks.Count(),
                    e.Tasks?.Count(t => !t.FinishedAt.HasValue) ?? 0,
                    _mapper.Map<TaskDto>(e.Tasks?.OrderByDescending(t => (t.FinishedAt ?? DateTime.Now) - t.CreatedAt).FirstOrDefault())
                )).Where(e => e.LastProject != null)
                .FirstOrDefault(r => r.User.Id == userId) ?? throw new ArgumentException("User doesn't have projects!");
        }

        public IEnumerable<ProjectInfoDto> ExecuteQuerySeven()
        {
            _projects = GetProjects();
            return _projects.Select(p => new ProjectInfoDto(
                    _mapper.Map<ProjectDto>(p),
                    _mapper.Map<TaskDto>(p.Tasks.OrderByDescending(t => t.Description.Length)?.FirstOrDefault()),
                    _mapper.Map<TaskDto>(p.Tasks.OrderBy(t => t.Name.Length)?.FirstOrDefault()),
                    p.Description.Length > 20 || p.Tasks.Count() < 3 ?
                        _projects.Where(pr => p.Author != null).Select(pr => pr.Author)
                        .Union(_projects.SelectMany(pr => pr.Tasks.Select(t => t.Performer)))
                        .Distinct(new UsersEqualityComparer())
                        .Where(u => u.TeamId == p.Team.Id)?.Count() ?? null : null)
                ).ToList();
        }

        private IEnumerable<Project> GetProjects()
        {
            var projects = _context.Projects.Include(p => p.Tasks).ThenInclude(t => t.Performer).Include(p => p.Team).Include(p => p.Author).ToList();
            return projects;
        }
    }
}
