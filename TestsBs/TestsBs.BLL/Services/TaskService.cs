﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services.Abstraction;
using TestsBs.Common.DTOs;
using TestsBs.DAL.Context;
using TestsBs.DAL.Entities;

namespace TestsBs.BLL.Services
{
    public class TaskService : BaseSerivce, ITaskService
    {
        public TaskService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public TaskDto AddTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);

            _context.Tasks.Add(taskEntity);

            _context.SaveChanges();
            return _mapper.Map<TaskDto>(GetTask(taskEntity.Id));
        }

        public TaskDto UpdateTask(TaskDto task)
        {
            var taskEntity = _mapper.Map<Task>(task);

            _context.Tasks.Update(taskEntity);

            _context.SaveChanges();
            return _mapper.Map<TaskDto>(GetTask(taskEntity.Id));
        }

        public void RemoveTask(int id)
        {
            var taskEntity = GetTask(id);

            if (taskEntity == null) throw new ArgumentException("No entity with this id!");

            _context.Tasks.Remove(taskEntity);

            _context.SaveChanges();
        }

        public IEnumerable<TaskDto> GetAllTasks()
        {
            return _mapper.Map<IEnumerable<TaskDto>>(GetTasks());
        }

        public TaskDto GetById(int id)
        {
            var taskEntity = GetTask(id);

            if (taskEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<TaskDto>(taskEntity);
        }

        public void SetTaskAsFinished(int taskId)
        {
            var task = GetTask(taskId);
            if (task == null)
                throw new ArgumentException("No task with this id");

            task.FinishedAt = DateTime.Now;

            _context.Tasks.Update(task);
            _context.SaveChanges();
        }

        public IEnumerable<TaskDto> GetUnfinishedTasksByUser(int userId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            if (user == null)
                throw new ArgumentException("No user with this id");
            var tasks = _context.Tasks.Where(t => t.PerformerId == user.Id && t.FinishedAt == null);
            return _mapper.Map<IEnumerable<TaskDto>>(tasks);
        }

        private IQueryable<Task> GetTasks()
        {
            return _context.Tasks.Include(t => t.Performer);
        }
        private Task GetTask(int id)
        {
            return GetTasks().FirstOrDefault(t => t.Id == id);
        }

    }
}
