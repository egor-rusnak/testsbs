﻿using AutoMapper;
using TestsBs.DAL.Context;

namespace TestsBs.BLL.Services.Abstraction
{
    public abstract class BaseSerivce
    {
        protected readonly ProjectDbContext _context;
        protected readonly IMapper _mapper;

        public BaseSerivce(ProjectDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
