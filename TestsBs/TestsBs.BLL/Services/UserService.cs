﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services.Abstraction;
using TestsBs.Common.DTOs.User;
using TestsBs.DAL.Context;
using TestsBs.DAL.Entities;

namespace TestsBs.BLL.Services
{
    public class UserService : BaseSerivce, IUserService
    {
        public UserService(ProjectDbContext context, IMapper mapper)
            : base(context, mapper) { }

        public UserDto AddUser(UserDto user)
        {
            var userEntity = _mapper.Map<User>(user);

            _context.Users.Add(userEntity);

            _context.SaveChanges();
            return _mapper.Map<UserDto>(GetUser(userEntity.Id));
        }

        public UserDto UpdateUser(UserDto user)
        {
            var userEntity = _mapper.Map<User>(user);

            _context.Users.Update(userEntity);

            _context.SaveChanges();
            return _mapper.Map<UserDto>(GetUser(userEntity.Id));
        }

        public void RemoveUser(int id)
        {
            var userEntity = GetUser(id);

            if (userEntity == null) throw new ArgumentException("No entity with this id!");

            _context.Users.Remove(userEntity);

            _context.SaveChanges();
        }

        public IEnumerable<UserDto> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDto>>(GetUsers());
        }

        public UserDto GetById(int id)
        {
            var teamEntity = GetUser(id);

            if (teamEntity == null)
                throw new ArgumentException("No entity with this id");

            return _mapper.Map<UserDto>(teamEntity);
        }

        private IQueryable<User> GetUsers()
        {
            return _context.Users.Include(u => u.Tasks).Include(u => u.Team);
        }

        private User GetUser(int id)
        {
            return GetUsers().FirstOrDefault(t => t.Id == id);
        }
    }
}
