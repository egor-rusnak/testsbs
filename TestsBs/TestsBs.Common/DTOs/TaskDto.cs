﻿using System;
using TestsBs.Common.DTOs.Abstraction;

namespace TestsBs.Common.DTOs
{
    public class TaskDto : BaseEntityDto
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
        public int ProjectId { get; set; }
        public string Description { get; set; }
        public int PerformerId { get; set; }

        public override string ToString()
        {
            return Id + "-" + Name;
        }
    }
}
