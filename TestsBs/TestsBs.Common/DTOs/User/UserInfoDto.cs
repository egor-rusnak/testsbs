﻿using TestsBs.Common.DTOs.Project;

namespace TestsBs.Common.DTOs.User
{
    public class UserInfoDto
    {
        public UserDto User { get; set; }
        public ProjectDto LastProject { get; set; }
        public int LastProjectTasksCount { get; set; }
        public int UnfinishedTasksCount { get; set; }
        public TaskDto LongestTask { get; set; }

        public UserInfoDto(UserDto user, ProjectDto lastProject, int lastProjectTasksCount, int unfinishedTasksCount, TaskDto longestTask)
        {
            User = user;
            LastProject = lastProject;
            LastProjectTasksCount = lastProjectTasksCount;
            UnfinishedTasksCount = unfinishedTasksCount;
            LongestTask = longestTask;
        }
    }
}
