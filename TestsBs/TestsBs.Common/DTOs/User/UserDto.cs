﻿using System;
using TestsBs.Common.DTOs.Abstraction;

namespace TestsBs.Common.DTOs.User
{
    public class UserDto : BaseEntityDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public DateTime RegisteredAt { get; set; }
        public string Email { get; set; }
        public int? TeamId { get; set; }
        public override string ToString()
        {
            return Id + "-" + FirstName + " " + LastName;
        }
    }
}
