﻿using System;
using System.Collections.Generic;
using TestsBs.Common.DTOs.Abstraction;
using TestsBs.Common.DTOs.User;

namespace TestsBs.Common.DTOs
{
    public class TeamDto : BaseEntityDto
    {
        public DateTime CreatedAt { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDto> Members { get; set; }
    }
}
