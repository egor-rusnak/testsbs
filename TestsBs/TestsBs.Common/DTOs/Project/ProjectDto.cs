﻿using System;
using System.Collections.Generic;
using TestsBs.Common.DTOs.Abstraction;

namespace TestsBs.Common.DTOs.Project
{
    public class ProjectDto : BaseEntityDto
    {
        public string Name { get; set; }
        public int AuthorId { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }
        public int TeamId { get; set; }
        public IEnumerable<TaskDto> Tasks { get; set; }
        public override string ToString()
        {
            return Id + "-" + Name;
        }
    }
}
