﻿namespace TestsBs.Common.DTOs.Project
{
    public class ProjectInfoDto
    {
        public ProjectDto Project { get; set; }
        public TaskDto LongestTask { get; set; }
        public TaskDto ShortestTask { get; set; }
        public int? UsersCountInTeam { get; set; }

        public ProjectInfoDto(ProjectDto project, TaskDto longestTask, TaskDto shortestTask, int? usersCountInTeam)
        {
            Project = project;
            LongestTask = longestTask;
            ShortestTask = shortestTask;
            UsersCountInTeam = usersCountInTeam;
        }
    }
}
