﻿namespace TestsBs.Common.DTOs.Abstraction
{
    public abstract class BaseEntityDto
    {
        public int Id { get; set; }
    }
}
