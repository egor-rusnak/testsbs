﻿using System;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services;
using TestsBs.DAL.Entities;
using Xunit;

namespace TestsBs.BLL.Tests
{
    public class TeamServiceTests : DbServicesTest
    {
        private readonly ITeamService _teamService;
        public TeamServiceTests() : base("teamDb")
        {
            _teamService = new TeamService(_context, _mapper);
        }

        [Fact]
        public void AddUserToTeam_AddedUserToTeam()
        {
            var team = _context.Teams.Add(new Team(0, "Team 1", DateTime.Now)).Entity;
            var user = _context.Users.Add(new User(0, "John", "Johnson", "", DateTime.Now, DateTime.Now, null)).Entity;
            _context.SaveChanges();

            _teamService.AddUserToTeam(team.Id, user.Id);

            Assert.True(_context.Users.FirstOrDefault(u => u.Id == user.Id).TeamId == team.Id);
        }

        [Fact]
        public void AddUserToTeam_UserAlreadyHaveTeam_ThrowsArgumentException()
        {
            var team = _context.Teams.Add(new Team(0, "Team 1", DateTime.Now)).Entity;
            var team2 = _context.Teams.Add(new Team(0, "Team 2", DateTime.Now)).Entity;
            var user = _context.Users.Add(new User(0, "John", "Johnson", "", DateTime.Now, DateTime.Now, team2.Id)).Entity;
            _context.SaveChanges();

            Assert.Throws<ArgumentException>(() => _teamService.AddUserToTeam(team.Id, user.Id));
        }
    }
}
