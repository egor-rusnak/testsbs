﻿using System;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services;
using TestsBs.DAL.Entities;
using Xunit;

namespace TestsBs.BLL.Tests
{
    public class TaskServiceTests : DbServicesTest
    {
        private readonly ITaskService _taskService;
        public TaskServiceTests() : base("TaskDb")
        {
            _taskService = new TaskService(_context, _mapper);
        }

        [Fact]
        public void SetTaskAsFinished_TaskSettedAsFinished()
        {
            var team = _context.Teams.Add(new Team(0, "T1", DateTime.Now)).Entity;
            var project = _context.Projects.Add(new Project(0, "P1", "", null, team.Id, DateTime.Now, DateTime.Now)).Entity;
            var task = _context.Tasks.Add(new Task(0, "t1", null, project.Id, "desc", DateTime.Now, null)).Entity;
            _context.Tasks.Add(new Task(0, "t2", null, project.Id, "desc", DateTime.Now, DateTime.Now));
            _context.SaveChanges();

            _taskService.SetTaskAsFinished(task.Id);

            var result = _context.Tasks.FirstOrDefault(t => t.Id == task.Id).FinishedAt;
            Assert.NotNull(result);
        }

        [Fact]
        public void SetTaskAsFinished_WrongTaskId_ThrowsArgumentException()
        {
            var team = _context.Teams.Add(new Team(0, "T1", DateTime.Now)).Entity;
            var project = _context.Projects.Add(new Project(0, "P1", "", null, team.Id, DateTime.Now, DateTime.Now)).Entity;
            _context.Tasks.Add(new Task(0, "t1", null, project.Id, "desc", DateTime.Now, null));
            _context.SaveChanges();

            Assert.Throws<ArgumentException>(() => _taskService.SetTaskAsFinished(4505));
        }

        [Fact]
        public void GetUnfinishedTasksByUser_UserWithUnfinishedTasks_ReturnedUnfinishedTasks()
        {
            var team = _context.Add(new Team(0, "Team1", DateTime.Now)).Entity;
            var user = _context.Add(new User(0, "user", "lname", "", DateTime.Now, DateTime.Now.AddYears(-5), team.Id)).Entity;
            var project = _context.Add(new Project(0, "Project1", "Description", user.Id, team.Id, DateTime.Now.AddMonths(3), DateTime.Now)).Entity;
            _context.AddRange(new[]
            {
                new Task(0,"task1",user.Id,project.Id,"desc1",DateTime.Now,DateTime.Now),
                new Task(0,"task2",user.Id,project.Id,"desc2",DateTime.Now,DateTime.Now),
                new Task(0,"task3",user.Id,project.Id,"desc3",DateTime.Now,null),
                new Task(0,"tas4",user.Id,project.Id,"desc4",DateTime.Now,null),
                new Task(0,"task5",user.Id,project.Id,"desc5",DateTime.Now,DateTime.Now),
                new Task(0,"task6",user.Id,project.Id,"desc6",DateTime.Now,DateTime.Now)
            });
            _context.SaveChanges();

            var result = _taskService.GetUnfinishedTasksByUser(user.Id);

            Assert.True(result.Count() == 2);
            Assert.All(result, t => Assert.Equal(user.Id, t.PerformerId));
        }

        [Fact]
        public void GetUnfinishedTasksByUser_NotExistingUser_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => _taskService.GetUnfinishedTasksByUser(99923));
        }

    }
}
