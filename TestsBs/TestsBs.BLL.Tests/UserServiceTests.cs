﻿using System;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services;
using TestsBs.Common.DTOs.User;
using TestsBs.DAL.Entities;
using Xunit;

namespace TestsBs.BLL.Tests
{
    public class UserServiceTests : DbServicesTest
    {
        private readonly IUserService _userService;

        public UserServiceTests() : base("UserDb")
        {
            _userService = new UserService(_context, _mapper);
        }


        [Theory]
        [InlineData("John", "Rachel", "10.01.2005", "10.01.2001")]
        [InlineData("Marry", "Jane", "10.01.2005", "10.01.2001")]
        public void AddUser_WithRightTeam_UserAdded(string userFname, string userLName, string registeredAt, string birthDay)
        {
            var team = _context.Teams.Add(new Team(0, "T1", DateTime.Now)).Entity;
            _context.SaveChanges();
            var user = new UserDto
            {
                FirstName = userFname,
                LastName = userLName,
                RegisteredAt = DateTime.Parse(registeredAt),
                TeamId = team.Id,
                BirthDay = DateTime.Parse(birthDay)
            };

            var resultUser = _userService.AddUser(user);

            Assert.NotNull(resultUser);
            Assert.Equal(team.Id, resultUser.TeamId);
        }
        [Theory]
        [InlineData("John", "Rachel", "10.01.2005", 300, "10.01.2001")]
        [InlineData("Marry", "Jane", "10.01.2005", 500, "10.01.2001")]
        public void AddUser_WithNoExistingTeam_UserAdded(string userFname, string userLName, string registeredAt, int? teamId, string birthDay)
        {
            var user = new UserDto
            {
                FirstName = userFname,
                LastName = userLName,
                RegisteredAt = DateTime.Parse(registeredAt),
                TeamId = teamId,
                BirthDay = DateTime.Parse(birthDay)
            };

            var resultUser = _userService.AddUser(user);

            Assert.NotNull(resultUser);
        }
    }
}
