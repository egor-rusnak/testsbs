﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using TestsBs.BLL.MappingProfiles;
using TestsBs.DAL.Context;

namespace TestsBs.BLL.Tests
{
    public class DbServicesTest : IDisposable
    {
        protected readonly IMapper _mapper;
        protected readonly ProjectDbContext _context;
        public DbServicesTest(string dbName)
        {
            _mapper = new Mapper(new MapperConfiguration(e =>
            {
                e.AddProfile<ProjectProfile>();
                e.AddProfile<TaskProfile>();
                e.AddProfile<TeamProfile>();
                e.AddProfile<UserProfile>();
            }));

            var options = new DbContextOptionsBuilder<ProjectDbContext>()
                .UseInMemoryDatabase(databaseName: dbName)
                .Options;

            _context = new ProjectDbContext(options);
        }

        public void Dispose()
        {
            _context.Database.EnsureDeleted();
            _context.Dispose();
        }
    }
}
