using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.BLL.Services;
using TestsBs.DAL.Entities;
using Xunit;

namespace TestsBs.BLL.Tests
{
    public class QueryServiceTests : DbServicesTest, IDisposable
    {
        private readonly IQueryService _queryService;
        public QueryServiceTests() : base("QueryDb")
        {
            _queryService = new QueryService(_mapper, _context);
        }

        [Fact]
        public void ExecuteQueryOne_UserWithTwoTasksInProject_ReturnsProjectWithTasksCount()
        {
            var user = _context.Users.Add(new User(0, "UserN", "UserL", "em", DateTime.Now, DateTime.Now, null)).Entity;
            var team = _context.Teams.Add(new Team(0, "Team1", DateTime.Now) { Members = new List<User>(new[] { user }) }).Entity;
            var project = _context.Projects.Add(new Project(0, "Project", "Description", 0, team.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            _context.Tasks.AddRange(new[] { new Task(0, "t1", user.Id, project.Id, "", DateTime.Now, null), new Task(0, "t2", user.Id, project.Id, "", DateTime.Now, null) });
            _context.SaveChanges();

            var result = _queryService.ExecuteQueryOne(user.Id);

            Assert.Single(result);
            Assert.Equal(2, result.FirstOrDefault().Value);
        }

        [Fact]
        public void ExecuteQueryTwo_UserWithTwoTasksInProjectWithNameSmallerThan45_ReturnsTwoTasksForUser()
        {
            var user = _context.Users.Add(new User(0, "UserN", "UserL", "em", DateTime.Now, DateTime.Now, null)).Entity;
            var team = _context.Teams.Add(new Team(0, "Team1", DateTime.Now)).Entity;
            var project = _context.Projects.Add(new Project(0, "Project", "Description", 0, team.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            _context.Tasks.AddRange(new[]
            {
                new Task(0, "t1", user.Id, project.Id, "", DateTime.Now, null),
                new Task(0, "t2", user.Id, project.Id, "", DateTime.Now, null),
                new Task(0,"12345678901234567890123456789012345678901234567890",user.Id,project.Id,"",DateTime.Now,null)
            });
            _context.SaveChanges();

            var result = _queryService.ExecuteQueryTwo(user.Id);

            Assert.True(result.Count() == 2);
            Assert.DoesNotContain(result, t => t.Name.Length > 45);
            Assert.DoesNotContain(result, t => t.PerformerId != user.Id);
        }

        [Fact]
        public void ExecuteQueryThree_UserWithTwoFinishedTasksInCurrentYear_ReturnsTwoFinishedTasksForUser()
        {
            var user = _context.Users.Add(new User(0, "UserN", "UserL", "em", DateTime.Now, DateTime.Now, null)).Entity;
            var team = _context.Teams.Add(new Team(0, "Team1", DateTime.Now)).Entity;
            var project = _context.Projects.Add(new Project(0, "Project", "Description", 0, team.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            _context.Tasks.AddRange(new[]
            {
                new Task(0, "t1", user.Id, project.Id, "", DateTime.Now, DateTime.Now),
                new Task(0, "t2", user.Id, project.Id, "", DateTime.Now, DateTime.Now),
                new Task(0,"t4",user.Id,project.Id,"",DateTime.Now.AddYears(-2),DateTime.Now.AddYears(-1)),
                new Task(0, "t3", user.Id, project.Id, "", DateTime.Now, null)
            });
            _context.SaveChanges();

            var result = _queryService.ExecuteQueryThree(user.Id);

            Assert.True(result.Count() == 2);
            Assert.Collection(result, t => Assert.Equal("t1", t.Value), t => Assert.Equal("t2", t.Value));
        }

        [Fact]
        public void ExecuteQueryFour_OneTeamWithUsersOlderThan10Years_ReturnsOneTeamWithUsersSortedByDescending()
        {
            var team1 = _context.Teams.Add(new Team(0, "Team1", DateTime.Now)).Entity;
            var team2 = _context.Teams.Add(new Team(0, "Team2", DateTime.Now)).Entity;
            var user1 = _context.Users.Add(new User(0, "UserN1", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var user2 = _context.Users.Add(new User(0, "UserN2", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var user3 = _context.Users.Add(new User(0, "UserN3", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team2.Id)).Entity;
            var user4 = _context.Users.Add(new User(0, "UserN4", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-5), team2.Id)).Entity;
            var project1 = _context.Projects.Add(new Project(0, "Project1", "Description", 0, team1.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            var project2 = _context.Projects.Add(new Project(0, "Project2", "Description", 0, team2.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            _context.Tasks.AddRange(new[]
            {
                new Task(0, "t1", user1.Id, project1.Id, "", DateTime.Now, DateTime.Now),
                new Task(0, "t2", user2.Id, project1.Id, "", DateTime.Now, DateTime.Now),
                new Task(0, "t4", user3.Id, project2.Id,"",DateTime.Now.AddYears(-2),DateTime.Now.AddYears(-1)),
                new Task(0, "t3", user4.Id, project2.Id, "", DateTime.Now, null)
            });
            _context.SaveChanges();

            var result = _queryService.ExecuteQueryFour();

            Assert.Single(result);
            var orderedList = result.First().Value.OrderByDescending(t => t.BirthDay).ToList();
            Assert.True(orderedList.SequenceEqual(result.First().Value));
            Assert.DoesNotContain(result.First().Value, t => t.BirthDay > DateTime.Now.AddYears(-10));
        }

        [Fact]
        public void ExecuteQueryFive_TwoUsersWithTwoTasks_ReturnsTwoUsersOrderedByFirstNameAndTasksOrderedByNameLength()
        {
            var team1 = _context.Teams.Add(new Team(0, "Team1", DateTime.Now)).Entity;
            var user1 = _context.Users.Add(new User(0, "UserN1", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var user2 = _context.Users.Add(new User(0, "UserN2", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var project = _context.Projects.Add(new Project(0, "Project1", "Description", 0, team1.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            _context.Tasks.AddRange(new[]
            {
                new Task(0, "t1�����", user1.Id, project.Id, "", DateTime.Now, DateTime.Now),
                new Task(0, "t223", user1.Id, project.Id, "", DateTime.Now, DateTime.Now),
                new Task(0, "t4�", user2.Id, project.Id,"",DateTime.Now.AddYears(-2),DateTime.Now.AddYears(-1)),
                new Task(0, "t3��", user2.Id, project.Id, "", DateTime.Now, null)
            });
            _context.SaveChanges();

            var result = _queryService.ExecuteQueryFive();

            var orderedListUsers = result.OrderBy(t => t.Key.FirstName).ToList();
            Assert.True(orderedListUsers.SequenceEqual(result));
            foreach (var user in result)
            {
                var orderedTasks = user.Value.OrderByDescending(t => t.Name.Length).ToList();
                Assert.True(orderedTasks.SequenceEqual(user.Value));
            }
            Assert.True(result.Count() == 2);
        }

        [Fact]
        public void ExecuteQuerySix_UsersWithTasksAndUnfinishedTasks_ReturnsUserByIdWithLastProjectAndTasksAndWithLongestTask()
        {
            var team1 = _context.Teams.Add(new Team(0, "Team1", DateTime.Now)).Entity;
            var user1 = _context.Users.Add(new User(0, "UserN1", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var user2 = _context.Users.Add(new User(0, "UserN2", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var project1 = _context.Projects.Add(new Project(0, "Project1", "Description", 0, team1.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            var project2 = _context.Projects.Add(new Project(0, "Project2", "Description", 0, team1.Id, DateTime.Now.AddDays(2), DateTime.Now.AddDays(-1))).Entity;
            _context.Tasks.AddRange(new[]
            {
                new Task(0, "t1�����", user1.Id, project1.Id, "", DateTime.Now, DateTime.Now),
                new Task(0, "t4�", user2.Id, project2.Id,"",DateTime.Now.AddYears(-2),DateTime.Now.AddYears(-1)),
                new Task(0, "t3��", user2.Id, project2.Id, "", DateTime.Now, null)
            });
            var longestTask = _context.Add(new Task(0, "t223", user1.Id, project2.Id, "", DateTime.Now, null)).Entity;
            _context.SaveChanges();

            var result = _queryService.ExecuteQuerySix(user1.Id);

            Assert.Equal(project1.Id, result.LastProject.Id);
            Assert.Equal(1, result.LastProjectTasksCount);
            Assert.Equal(1, result.UnfinishedTasksCount);
            Assert.Equal(user1.Id, result.User.Id);
            Assert.Equal(longestTask.Id, result.LongestTask.Id);
        }

        [Fact]
        public void ExecuteQuerySeven_TwoProjectsWithTasks_ReturnsProjectsAndTaskWithLongestDescAndTaskWithShortestNameWhereProjectDescriptionLengthMoreThan20OrTasksCountSmallerThan3()
        {
            var team1 = _context.Teams.Add(new Team(0, "Team1", DateTime.Now)).Entity;
            var user1 = _context.Users.Add(new User(0, "UserN1", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var user2 = _context.Users.Add(new User(0, "UserN2", "UserL", "em", DateTime.Now, DateTime.Now.AddYears(-15), team1.Id)).Entity;
            var project1 = _context.Projects.Add(new Project(0, "Project1", "Description", 0, team1.Id, DateTime.Now.AddDays(3), DateTime.Now)).Entity;
            var project2 = _context.Projects.Add(new Project(0, "Project2", "Description", 0, team1.Id, DateTime.Now.AddDays(2), DateTime.Now.AddDays(-1))).Entity;
            _context.Tasks.AddRange(new[]
            {
                new Task(0, "Task3132dfafsd", user1.Id, project1.Id, "desc", DateTime.Now, DateTime.Now),
                new Task(0, "t4�", user2.Id, project2.Id,"",DateTime.Now.AddYears(-2),DateTime.Now.AddYears(-1)),
                new Task(0, "t3��", user2.Id, project2.Id, "", DateTime.Now, null),
                new Task(0, "t3��", user2.Id, project2.Id, "", DateTime.Now, null)
            });
            var taskToCheck = _context.Add(new Task(0, "shortestName", user1.Id, project1.Id, "longestDesc", DateTime.Now, null)).Entity;
            _context.SaveChanges();

            var result = _queryService.ExecuteQuerySeven();

            Assert.True(result.Count() == 2);
            Assert.Equal(taskToCheck.Id, result.First().LongestTask.Id);
            Assert.Equal(taskToCheck.Id, result.First().ShortestTask.Id);
            Assert.Collection(result, p => Assert.True(p.UsersCountInTeam == 2), p => Assert.True(p.UsersCountInTeam == null));
        }
    }
}
