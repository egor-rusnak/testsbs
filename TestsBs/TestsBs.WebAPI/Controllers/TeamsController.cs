﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TestsBs.BLL.Interfaces;
using TestsBs.Common.DTOs;

namespace TestsBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TeamDto>> GetAll()
        {
            return Ok(_teamService.GetAllTeams());
        }
        [HttpGet("{id}")]
        public ActionResult<TeamDto> Get(int id)
        {
            try
            {
                var team = _teamService.GetById(id);
                return Ok(team);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message); }
        }

        [HttpPost]
        public ActionResult<TeamDto> CreateTeam([FromBody] TeamDto team)
        {
            if (!ModelState.IsValid)
                return BadRequest("Bad data");

            try
            {
                var result = _teamService.AddTeam(team);
                return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPut]
        public ActionResult<TeamDto> UpdateTeam([FromBody] TeamDto team)
        {
            if (!ModelState.IsValid)
                return BadRequest("Bad data");

            try
            {
                var result = _teamService.UpdateTeam(team);
                return Ok(result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTeam(int id)
        {
            try
            {
                _teamService.RemoveTeam(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }
    }
}
