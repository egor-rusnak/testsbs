﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.BLL.Interfaces;
using TestsBs.Common.DTOs;
using TestsBs.Common.DTOs.Project;
using TestsBs.Common.DTOs.User;

namespace TestsBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class QueriesController : ControllerBase
    {
        private readonly IQueryService _queryService;

        public QueriesController(IQueryService queryService)
        {
            _queryService = queryService;
        }

        [HttpGet("First/{id}")]
        public ActionResult<Dictionary<ProjectDto, int>> FirstQuery(int id)
        {
            var result = _queryService.ExecuteQueryOne(id);
            return Ok(result.Select(e => e));
        }

        [HttpGet("Second/{id}")]
        public ActionResult<IEnumerable<TaskDto>> SecondQuery(int id)
        {
            var result = _queryService.ExecuteQueryTwo(id);

            return Ok(result);
        }

        [HttpGet("Third/{id}")]
        public ActionResult<IEnumerable<KeyValuePair<int, string>>> ThirdQuery(int id)
        {
            var result = _queryService.ExecuteQueryThree(id);

            return Ok(result);
        }

        [HttpGet("Fourth")]
        public ActionResult<IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>> FourthQuery()
        {
            var result = _queryService.ExecuteQueryFour();

            return Ok(result);
        }

        [HttpGet("Fifth")]
        public ActionResult<IEnumerable<KeyValuePair<UserDto, IEnumerable<TaskDto>>>> FifthQuery()
        {
            var result = _queryService.ExecuteQueryFive();

            return Ok(result);
        }

        [HttpGet("Sixth/{id}")]
        public ActionResult<UserInfoDto> SixthQuery(int id)
        {
            try
            {
                var result = _queryService.ExecuteQuerySix(id);
                return Ok(result);
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet("Seventh")]
        public ActionResult<IEnumerable<ProjectInfoDto>> SeventhQuery()
        {
            var result = _queryService.ExecuteQuerySeven();
            return Ok(result);
        }
    }
}
