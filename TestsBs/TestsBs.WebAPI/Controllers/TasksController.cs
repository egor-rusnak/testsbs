﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TestsBs.BLL.Interfaces;
using TestsBs.Common.DTOs;

namespace TestsBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<TaskDto>> GetAll()
        {
            return Ok(_taskService.GetAllTasks());
        }

        [HttpGet("{id}")]
        public ActionResult<TaskDto> Get(int id)
        {
            try
            {
                var task = _taskService.GetById(id);
                return Ok(task);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPost]
        public ActionResult<TaskDto> CreateTask([FromBody] TaskDto task)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");

            try
            {
                var result = _taskService.AddTask(task);
                return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPut]
        public ActionResult<TaskDto> UpdateTask([FromBody] TaskDto task)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");

            try
            {
                var result = _taskService.UpdateTask(task);
                return Ok(result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTask(int id)
        {
            try
            {
                _taskService.RemoveTask(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPatch("finished/{id}")]
        public IActionResult SetTaskAsFinished(int id)
        {
            try
            {
                _taskService.SetTaskAsFinished(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpGet("unfinishedByUser/{userId}")]
        public ActionResult<IEnumerable<TaskDto>> GetUnfinishedTasksByUserId(int userId)
        {
            try
            {
                var result = _taskService.GetUnfinishedTasksByUser(userId);
                return Ok(result);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }
    }
}
