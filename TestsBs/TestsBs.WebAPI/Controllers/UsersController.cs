﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TestsBs.BLL.Interfaces;
using TestsBs.Common.DTOs.Project;
using TestsBs.Common.DTOs.User;

namespace TestsBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> GetAll()
        {
            return Ok(_userService.GetAllUsers());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDto> Get(int id)
        {
            try
            {
                var user = _userService.GetById(id);
                return Ok(user);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPost]
        public ActionResult<ProjectDto> CreateUser([FromBody] UserDto user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Bad data");

            try
            {
                var result = _userService.AddUser(user);
                return CreatedAtAction(nameof(Get), new
                {
                    id = result.Id
                }, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPut]
        public ActionResult<ProjectDto> UpdateUser([FromBody] UserDto user)
        {
            if (!ModelState.IsValid)
                return BadRequest("Bad data");

            try
            {
                var result = _userService.UpdateUser(user);
                return Ok(result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteUser(int id)
        {
            try
            {
                _userService.RemoveUser(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }        }

    }
}
