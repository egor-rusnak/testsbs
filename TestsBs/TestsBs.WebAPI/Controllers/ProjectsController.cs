﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TestsBs.BLL.Interfaces;
using TestsBs.Common.DTOs.Project;

namespace TestsBs.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<ProjectDto>> GetAll()
        {
            return Ok(_projectService.GetAllProjects());
        }

        [HttpGet("{id}")]
        public ActionResult<ProjectDto> Get(int id)
        {
            try
            {
                var project = _projectService.GetById(id);
                return Ok(project);
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPost]
        public ActionResult<ProjectDto> CreateProject([FromBody] ProjectDto project)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");

            try
            {
                var result = _projectService.AddProject(project);
                return CreatedAtAction(nameof(Get), new { id = result.Id }, result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpPut]
        public ActionResult<ProjectDto> UpdateProject([FromBody] ProjectDto project)
        {
            if (!ModelState.IsValid)
                return BadRequest("The data is not valid!");
            try
            {
                var result = _projectService.UpdateProject(project);
                return Ok(result);
            }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteProject(int id)
        {
            try
            {
                _projectService.RemoveProject(id);
                return NoContent();
            }
            catch (ArgumentException ex) { return NotFound(ex.Message); }
            catch (Exception ex) { return BadRequest(ex.Message + "\n" + ex.InnerException?.Message); }
        }

    }
}
