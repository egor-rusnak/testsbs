﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using TestsBs.DAL.Context;
using TestsBs.DAL.Entities;
using Xunit;

namespace TestsBs.WebAPI.IntegrationTests
{
    public class UsersControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private CustomWebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;
        public UsersControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveUser_UserRemovedwithNoContentCode()
        {
            int idToRemove = 0;
            using (var context = _factory.Services.CreateScope().ServiceProvider.GetService<ProjectDbContext>())
            {
                var entity = context.Add(new User(0, "FirstName", "LastName", "email", DateTime.Now, DateTime.Now, null)).Entity;
                context.SaveChanges();
                idToRemove = entity.Id;
            }

            var response = await _client.DeleteAsync($"api/users/{idToRemove}");

            response.EnsureSuccessStatusCode();
            Assert.Equal(System.Net.HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveUser_WhenNotExist_NotFoundCodeReturned()
        {
            int idToRemove = 0;
            using (var context = _factory.Services.CreateScope().ServiceProvider.GetService<ProjectDbContext>())
            {
                var entity = context.Add(new User(0, "FirstName", "LastName", "email", DateTime.Now, DateTime.Now, null)).Entity;
                context.SaveChanges();
                idToRemove = entity.Id;
            }

            idToRemove++;
            var response = await _client.DeleteAsync($"api/users/{idToRemove}");

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
