﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using TestsBs.Common.DTOs;
using TestsBs.DAL.Context;
using TestsBs.DAL.Entities;
using Xunit;
using Task = TestsBs.DAL.Entities.Task;

namespace TestsBs.WebAPI.IntegrationTests
{
    public class TasksControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        private readonly CustomWebApplicationFactory<Startup> _factory;
        public TasksControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async System.Threading.Tasks.Task SetTaskAsFinished_TaskSettedAsFinishedWithCodeNoContent()
        {
            int taskId = 0;
            using (var context = _factory.Services.CreateScope().ServiceProvider.GetRequiredService<ProjectDbContext>())
            {
                var team = context.Teams.Add(new Team(0, "T1", DateTime.Now)).Entity;
                var project = context.Projects.Add(new Project(0, "P1", "", null, team.Id, DateTime.Now, DateTime.Now)).Entity;
                var task = context.Tasks.Add(new Task(0, "t1", null, project.Id, "desc", DateTime.Now, null)).Entity;
                context.Tasks.Add(new Task(0, "t2", null, project.Id, "desc", DateTime.Now, DateTime.Now));
                context.SaveChanges();
                taskId = task.Id;
            }

            var response = await _client.PatchAsync($"api/tasks/finished/{taskId}", null);

            response.EnsureSuccessStatusCode();
            Assert.Equal(System.Net.HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task SetTaskAsFinished_WrongTaskId_ReturnsNotFoundCode()
        {
            int taskId = 0;
            using (var context = _factory.Services.CreateScope().ServiceProvider.GetRequiredService<ProjectDbContext>())
            {
                var team = context.Teams.Add(new Team(0, "T1", DateTime.Now)).Entity;
                var project = context.Projects.Add(new Project(0, "P1", "", null, team.Id, DateTime.Now, DateTime.Now)).Entity;
                var task = context.Tasks.Add(new Task(0, "t1", null, project.Id, "desc", DateTime.Now, null)).Entity;
                context.SaveChanges();
                taskId = task.Id;
            }

            taskId++;
            var response = await _client.PatchAsync($"api/tasks/finished/{taskId}", null);

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveTask_TaskRemovedWithNoContentCode()
        {
            int idToRemove = 0;
            using (var context = _factory.Services.CreateScope().ServiceProvider.GetService<ProjectDbContext>())
            {
                var team = context.Teams.Add(new Team(0, "T1", DateTime.Now)).Entity;
                var project = context.Projects.Add(new Project(0, "P1", "", null, team.Id, DateTime.Now, DateTime.Now)).Entity;
                var task = context.Tasks.Add(new Task(0, "t1", null, project.Id, "desc", DateTime.Now, null)).Entity;
                context.SaveChanges();
                idToRemove = task.Id;
            }

            var response = await _client.DeleteAsync($"api/tasks/{idToRemove}");

            response.EnsureSuccessStatusCode();
            Assert.Equal(System.Net.HttpStatusCode.NoContent, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task RemoveTask_WhenNotExist_NotFoundCodeReturned()
        {
            int idToRemove = 0;
            using (var context = _factory.Services.CreateScope().ServiceProvider.GetService<ProjectDbContext>())
            {
                var team = context.Teams.Add(new Team(0, "T1", DateTime.Now)).Entity;
                var project = context.Projects.Add(new Project(0, "P1", "", null, team.Id, DateTime.Now, DateTime.Now)).Entity;
                var task = context.Tasks.Add(new Task(0, "t1", null, project.Id, "desc", DateTime.Now, null)).Entity;
                context.SaveChanges();
                idToRemove = task.Id;
            }

            idToRemove++;
            var response = await _client.DeleteAsync($"api/tasks/{idToRemove}");

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnfinishedTasksByUser_UserWithUnfinishedTasks_ReturnsCollectionOfTasksWithOkCode()
        {
            int userId = 0;
            using (var context = _factory.Services.CreateScope().ServiceProvider.GetService<ProjectDbContext>())
            {
                var team = context.Add(new Team(0, "Team1", DateTime.Now)).Entity;
                var user = context.Add(new User(0, "user", "lname", "", DateTime.Now, DateTime.Now.AddYears(-5), team.Id)).Entity;
                var project = context.Add(new Project(0, "Project1", "Description", user.Id, team.Id, DateTime.Now.AddMonths(3), DateTime.Now)).Entity;
                context.AddRange(new[]
                {
                    new Task(0,"task1",user.Id,project.Id,"desc1",DateTime.Now,DateTime.Now),
                    new Task(0,"task2",user.Id,project.Id,"desc2",DateTime.Now,DateTime.Now),
                    new Task(0,"task3",user.Id,project.Id,"desc3",DateTime.Now,null),
                    new Task(0,"tas4",user.Id,project.Id,"desc4",DateTime.Now,null),
                    new Task(0,"task5",user.Id,project.Id,"desc5",DateTime.Now,DateTime.Now),
                    new Task(0,"task6",user.Id,project.Id,"desc6",DateTime.Now,DateTime.Now)
                });
                context.SaveChanges();
                userId = user.Id;
            }

            var response = await _client.GetAsync($"api/tasks/unfinishedByUser/{userId}");

            response.EnsureSuccessStatusCode();
            var tasksResult = await response.Content.ReadFromJsonAsync<List<TaskDto>>();
            Assert.True(tasksResult.Count() == 2);
            Assert.All(tasksResult, t => Assert.Equal(userId, t.PerformerId));
        }

        [Fact]
        public async System.Threading.Tasks.Task GetUnfinishedTasksByUser_NotExistingUser_ReturnsNotFoundCode()
        {
            var response = await _client.GetAsync($"api/tasks/unfinishedByUser/{99832}");

            Assert.Equal(System.Net.HttpStatusCode.NotFound, response.StatusCode);
        }
    }
}
