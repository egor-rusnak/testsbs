﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using TestsBs.Common.DTOs.Project;
using Xunit;

namespace TestsBs.WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private readonly HttpClient _client;
        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task CreateProject_ReturnsCreatedProjectWithSuccessStatusCode()
        {
            var project = new ProjectDto()
            {
                Name = "Project 1",
                Description = "Description 1",
                Deadline = DateTime.Now.AddMonths(3),
                CreatedAt = DateTime.Now
            };

            var response = await _client.PostAsJsonAsync("api/projects", project);
            response.EnsureSuccessStatusCode();

            var projectAnswer = await response.Content.ReadFromJsonAsync<ProjectDto>();
            Assert.Equal(project.Name, projectAnswer.Name);
            Assert.Equal(project.Description, projectAnswer.Description);
            Assert.Equal(project.Deadline, projectAnswer.Deadline);
            Assert.NotEqual(DateTime.MinValue, projectAnswer.CreatedAt);
        }

        [Fact]
        public async Task CreateProject_BadBody_ReturnsBadRequestCode()
        {
            var response = await _client.PostAsJsonAsync("api/projects", "someDataDSASDFAs");

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
        }
    }
}
