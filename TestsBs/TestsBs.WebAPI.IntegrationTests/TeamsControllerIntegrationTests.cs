﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using TestsBs.Common.DTOs;
using Xunit;

namespace TestsBs.WebAPI.IntegrationTests
{
    public class TeamsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>, IDisposable
    {
        private CustomWebApplicationFactory<Startup> _factory;
        private readonly HttpClient _client;
        public TeamsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = _factory.CreateClient();
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        [Fact]
        public async Task CreateTeam_TeamCreatedWithResponseTeam()
        {
            var team = new TeamDto { Name = "New Good Team", CreatedAt = DateTime.Now };

            var response = await _client.PostAsJsonAsync("api/teams/", team);

            response.EnsureSuccessStatusCode();
            var teamAsnwer = await response.Content.ReadFromJsonAsync<TeamDto>();
            Assert.Equal(team.Name, teamAsnwer.Name);
            Assert.Equal(team.CreatedAt, teamAsnwer.CreatedAt);
        }

        [Fact]
        public async Task CreateTeam_BadBody_ReturnedBadRequestCode()
        {
            var response = await _client.PostAsJsonAsync<string>("api/teams/", "sadfasdffd");

            Assert.Equal(System.Net.HttpStatusCode.BadRequest, response.StatusCode);
        }

    }
}
