﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using TestsBs.DAL.Context;

namespace TestsBs.WebAPI.IntegrationTests
{
    public class CustomWebApplicationFactory<TStartup> : WebApplicationFactory<Startup>
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(ConfigureServices);
        }

        private void ConfigureServices(WebHostBuilderContext webHostBuilderContext, IServiceCollection services)
        {

            var dbContextService = services.SingleOrDefault(d => d.ServiceType == typeof(DbContextOptions<ProjectDbContext>));
            if (dbContextService != null)
            {
                services.Remove(dbContextService);
            }

            var dbName = Guid.NewGuid().ToString();

            services.AddDbContext<ProjectDbContext>(options =>
            {
                options.UseInMemoryDatabase(dbName);
            });
        }
    }
}
