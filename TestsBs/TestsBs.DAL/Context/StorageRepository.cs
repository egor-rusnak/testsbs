﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestsBs.DAL.Entities.Abstraction;
using TestsBs.DAL.Interfaces;

namespace TestsBs.DAL.Context
{
    public class StorageRepository<T> : IRepository<T> where T : BaseEntity<T>
    {
        private readonly Storage<T> _storage;
        private List<T> _list;
        public StorageRepository(Storage<T> storage)
        {
            _storage = storage;
            _list = _storage.List;
        }

        public T Create(T entity)
        {
            return _storage.Add(entity);
        }

        public IEnumerable<T> CreateRange(IEnumerable<T> entities)
        {
            foreach (var user in entities)
            {
                yield return Create(user);
            }
        }

        public void Delete(T entity)
        {
            _storage.Remove(entity.Id);
        }

        public void Delete(int id)
        {
            _storage.Remove(id);
        }

        public IEnumerable<T> GetAll()
        {
            return _list.ToList();
        }

        public IEnumerable<T> GetBy(Func<T, bool> filter)
        {
            return _list.AsReadOnly().Where(filter);
        }

        public T GetById(int id)
        {
            return _list.FirstOrDefault(e => e.Id == id);
        }

        public T Update(T entity)
        {
            return _storage.Update(entity);
        }
    }
}
