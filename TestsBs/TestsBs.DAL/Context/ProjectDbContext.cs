﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using TestsBs.DAL.Entities;

namespace TestsBs.DAL.Context
{
    public class ProjectDbContext : DbContext
    {

        public DbSet<User> Users { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Project> Projects { get; set; }

        public ProjectDbContext(DbContextOptions<ProjectDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasMany(u => u.Tasks).WithOne(t => t.Performer)
                .OnDelete(DeleteBehavior.NoAction);
            modelBuilder.Entity<Project>().HasOne(p => p.Author).WithMany().OnDelete(DeleteBehavior.SetNull);

            SeedData(modelBuilder);
        }

        private void SeedData(ModelBuilder modelBuilder)
        {
            var teams = new List<Team>()
            {
                new Team(1, "Bananas", DateTime.Now.AddDays(new Random().Next() % 30 - 15)),
                new Team(2, "Changes", DateTime.Now.AddDays(new Random().Next() % 30 - 15)),
                new Team(3, "Packages", DateTime.Now.AddDays(new Random().Next() % 30 - 15)),
                new Team(4, "asdas", DateTime.Now.AddDays(new Random().Next() % 30 - 15))
            };

            var users = new List<User>()
            {
                new User(1, "John", "Jonson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 1),
                new User(2, "Marchel", "Davidson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 3),
                new User(3, "Anchey", "Brownly", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 3),
                new User(4, "Boris", "Apostolov", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 2),
                new User(5, "Grace", "Donalds", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 2),
                new User(6, "George", "Jakson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), null),
                new User(7, "Annet", "Wonderson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), null),
                new User(8, "Marcus", "Jonson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500), 1),
                new User(9, "Marcus", "Jonson", "", DateTime.Now.AddDays(new Random().Next() % 3000 - 3500), DateTime.Now.AddYears(-100), 4)
            };
            var projects = new List<Project>
            {
                new Project(1, "ProjName1", "Des1", 1, 1, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500)),
                new Project(2, "ProjName2", "Des2", 5, 1, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500)),
                new Project(3, "ProjName3", "Des3", 8, 2, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500)),
                new Project(4, "ProjName4", "Des4", 7, 3, DateTime.Now.AddDays(new Random().Next() % 3000 - 2500), DateTime.Now.AddDays(new Random().Next() % 6000 - 6500))
            };

            var tasks = new List<Task>
            {
                new Task(1, "task1", 1, 1, "no", DateTime.Now.AddDays(new Random().Next() % 30 - 15), null),
                new Task(2, "task2", 1, 1, "no", DateTime.Now.AddDays(new Random().Next() % 30 - 15), DateTime.Now.AddDays(new Random().Next() % 30 - 15)),
                new Task(3, "task3", 1, 1, "no", DateTime.Now.AddDays(new Random().Next() % 30 - 15), null)
            };

            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

    }
}
