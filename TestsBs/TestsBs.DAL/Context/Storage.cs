﻿using System.Collections.Generic;
using System.Linq;
using TestsBs.DAL.Entities.Abstraction;

namespace TestsBs.DAL.Context
{
    public class Storage<T> where T : BaseEntity<T>
    {
        public List<T> List { get; set; } = new List<T>();

        public T Add(T entity)
        {
            entity.Id = GenerateId();
            List.Add(entity);
            return entity;
        }
        public T Update(T entity)
        {
            var entityToUpdate = List.FirstOrDefault(t => t.Id == entity.Id);
            if (entityToUpdate != null)
                entityToUpdate.Update(entity);
            return entityToUpdate;
        }
        public void Remove(int id)
        {
            var entity = List.FirstOrDefault(t => t.Id == id);
            List.Remove(entity);
        }

        public int GenerateId()
        {
            var id = List.Count == 0 ? 1 : List.Max(t => t.Id) + 1;
            return id;
        }
    }
}
