﻿using System;
using System.Collections.Generic;
using TestsBs.DAL.Entities.Abstraction;

namespace TestsBs.DAL.Interfaces
{
    public interface IRepository<T> where T : BaseEntity<T>
    {
        T Create(T entity);
        IEnumerable<T> CreateRange(IEnumerable<T> entities);
        T Update(T entity);
        void Delete(T entity);
        void Delete(int id);
        T GetById(int id);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetBy(Func<T, bool> filter);
    }
}
