﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace TestsBs.DAL.Entities.Comparers
{
    public class UsersEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
        {
            return x.Id == y.Id;
        }

        public int GetHashCode([DisallowNull] User obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}
