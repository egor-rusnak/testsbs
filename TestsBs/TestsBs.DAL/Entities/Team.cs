﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestsBs.DAL.Entities.Abstraction;

namespace TestsBs.DAL.Entities
{
    public class Team : BaseEntity<Team>
    {
        [StringLength(100)]
        public string Name { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public IEnumerable<User> Members { get; set; }

        public Team(int id, string name, DateTime createdAt) : base(id)
        {
            Name = name;
            CreatedAt = createdAt;
        }

        public override void Update(Team another)
        {
            Name = another.Name;
            CreatedAt = another.CreatedAt;
            Members = another.Members;
        }
    }
}
