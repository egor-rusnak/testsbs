﻿using System;

namespace TestsBs.DAL.Entities.Abstraction
{
    public abstract class BaseEntity<T> : IEquatable<BaseEntity<T>> where T : class
    {
        public int Id { get; set; }
        public BaseEntity(int id)
        {
            Id = id;
        }
        public abstract void Update(T another);

        public bool Equals(BaseEntity<T> other)
        {
            return this.Id == other.Id;
        }
    }
}
