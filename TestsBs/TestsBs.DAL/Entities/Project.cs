﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestsBs.DAL.Entities.Abstraction;

namespace TestsBs.DAL.Entities
{
    public class Project : BaseEntity<Project>
    {
        [StringLength(200)]
        public string Name { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        public int? AuthorId { get; set; }
        public User Author { get; set; }
        public int TeamId { get; set; }
        public Team Team { get; set; }
        [DataType(DataType.Date)]
        public DateTime Deadline { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        public IEnumerable<Task> Tasks { get; set; }

        public Project(int id, string name, string description, int? authorId, int teamId, DateTime deadline, DateTime createdAt) : base(id)
        {
            Description = description;
            Name = name;
            AuthorId = authorId;
            TeamId = teamId;
            Deadline = deadline;
            CreatedAt = createdAt == DateTime.MinValue ? DateTime.Now : createdAt;
        }

        public override void Update(Project another)
        {
            Description = another.Description;
            Name = another.Name;
            AuthorId = another.AuthorId;
            TeamId = another.TeamId;
            Tasks = another.Tasks;
            Team = another.Team;
            Author = another.Author;
            Deadline = another.Deadline;
            CreatedAt = another.CreatedAt;
        }
    }
}
