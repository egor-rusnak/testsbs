﻿using System;
using System.ComponentModel.DataAnnotations;
using TestsBs.DAL.Entities.Abstraction;

namespace TestsBs.DAL.Entities
{
    public class Task : BaseEntity<Task>
    {
        [StringLength(200)]
        public string Name { get; set; }
        public int? PerformerId { get; set; }
        public User Performer { get; set; }
        public int ProjectId { get; set; }
        [StringLength(500)]
        public string Description { get; set; }
        [DataType(DataType.Date)]
        public DateTime CreatedAt { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        public DateTime? FinishedAt { get; set; }

        public Task(int id, string name, int? performerId, int projectId, string description, DateTime createdAt, DateTime? finishedAt) : base(id)
        {
            PerformerId = performerId;
            ProjectId = projectId;
            Description = description;
            CreatedAt = createdAt;
            FinishedAt = finishedAt;
            Name = name;
        }

        public override void Update(Task another)
        {
            PerformerId = another.PerformerId;
            Performer = another.Performer;
            ProjectId = another.ProjectId;
            Description = another.Description;
            CreatedAt = another.CreatedAt;
            FinishedAt = another.FinishedAt;
            Name = another.Name;
        }
    }
}
