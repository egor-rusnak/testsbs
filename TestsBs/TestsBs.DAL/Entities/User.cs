﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TestsBs.DAL.Entities.Abstraction;

namespace TestsBs.DAL.Entities
{
    public class User : BaseEntity<User>
    {
        [StringLength(100)]
        public string FirstName { get; set; }
        [StringLength(100)]
        public string LastName { get; set; }
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.Date)]
        public DateTime RegisteredAt { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        public DateTime BirthDay { get; set; }
        public int? TeamId { get; set; }
        public Team Team { get; set; }
        public IEnumerable<Task> Tasks { get; set; }

        public User(int id, string firstName, string lastName, string email, DateTime registeredAt, DateTime birthDay, int? teamId) : base(id)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            RegisteredAt = registeredAt;
            BirthDay = birthDay;
            TeamId = teamId;
        }

        public override string ToString()
        {
            return Id + "-" + FirstName + " " + LastName;
        }

        public override void Update(User another)
        {
            FirstName = another.FirstName;
            LastName = another.LastName;
            Email = another.Email;
            RegisteredAt = another.RegisteredAt;
            BirthDay = another.BirthDay;
            TeamId = another.TeamId;
            Team = another.Team;
            Tasks = another.Tasks;
        }
    }
}
