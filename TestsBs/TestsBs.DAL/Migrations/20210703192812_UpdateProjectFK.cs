﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace TestsBs.DAL.Migrations
{
    public partial class UpdateProjectFK : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2005, 2, 18, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(2055), new DateTime(2015, 7, 19, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(2029) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2005, 1, 4, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4968), new DateTime(2020, 7, 3, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4931) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2015, 1, 19, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5018), new DateTime(2018, 6, 5, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4996) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2016, 11, 20, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5066), new DateTime(2018, 10, 21, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5044) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5209));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 7, 4, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8197), new DateTime(2021, 6, 28, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8231) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8662));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 17, 22, 28, 12, 422, DateTimeKind.Local).AddTicks(6646));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8275));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 6, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8327));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 4, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8352));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 10, 9, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8549), new DateTime(2016, 2, 29, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8526) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2010, 11, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1456), new DateTime(2015, 4, 9, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1420) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2018, 3, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1507), new DateTime(2019, 3, 30, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1486) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 2, 1, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1555), new DateTime(2012, 11, 7, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1533) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 8, 7, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1602), new DateTime(2014, 9, 26, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1580) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2020, 1, 29, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1655), new DateTime(2014, 10, 27, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1633) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 1, 30, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1704), new DateTime(2018, 11, 20, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1682) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 9, 25, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1808), new DateTime(2015, 9, 23, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1786) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1855), new DateTime(2013, 2, 24, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1832) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2004, 4, 30, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(8240), new DateTime(2018, 2, 10, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(8215) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2012, 11, 6, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(1122), new DateTime(2014, 10, 24, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(1087) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2011, 3, 19, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(1172), new DateTime(2016, 12, 18, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(1150) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2017, 7, 26, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(1220), new DateTime(2014, 11, 29, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(1198) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 27, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(1358));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 6, 30, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(4304), new DateTime(2021, 7, 16, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(4341) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 14, 22, 26, 43, 41, DateTimeKind.Local).AddTicks(4876));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 11, 22, 26, 43, 37, DateTimeKind.Local).AddTicks(5884));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 11, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(4558));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 20, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(4605));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 26, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(4632));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 7, 30, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(4809), new DateTime(2013, 1, 28, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(4784) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 12, 31, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7659), new DateTime(2012, 9, 5, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7621) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2018, 11, 24, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7710), new DateTime(2014, 7, 13, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7688) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 9, 10, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7816), new DateTime(2013, 8, 8, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7793) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2015, 8, 24, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7864), new DateTime(2015, 5, 14, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7843) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 10, 14, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7914), new DateTime(2019, 8, 7, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7894) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 10, 21, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7962), new DateTime(2017, 6, 9, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7940) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2016, 1, 12, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(8009), new DateTime(2012, 6, 10, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(7986) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(8054), new DateTime(2012, 11, 1, 22, 26, 43, 40, DateTimeKind.Local).AddTicks(8034) });
        }
    }
}
