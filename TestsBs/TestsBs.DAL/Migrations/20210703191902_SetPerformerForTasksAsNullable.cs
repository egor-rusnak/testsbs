﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace TestsBs.DAL.Migrations
{
    public partial class SetPerformerForTasksAsNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PerformerId",
                table: "Tasks",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2006, 6, 29, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5966), new DateTime(2016, 4, 10, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5942) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2012, 7, 30, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8735), new DateTime(2015, 4, 8, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8700) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2014, 11, 11, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8785), new DateTime(2020, 4, 26, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8764) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2010, 4, 15, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8834), new DateTime(2020, 12, 9, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8811) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8976));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 7, 14, 22, 19, 2, 175, DateTimeKind.Local).AddTicks(2119), new DateTime(2021, 7, 7, 22, 19, 2, 175, DateTimeKind.Local).AddTicks(2156) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 30, 22, 19, 2, 175, DateTimeKind.Local).AddTicks(2585));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 22, 19, 2, 171, DateTimeKind.Local).AddTicks(3214));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 29, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2152));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2202));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 5, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2228));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2010, 1, 8, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2411), new DateTime(2016, 7, 10, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2387) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2007, 9, 12, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5385), new DateTime(2013, 2, 6, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5350) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 10, 6, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5437), new DateTime(2017, 12, 14, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5415) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 6, 13, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5485), new DateTime(2012, 1, 11, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5463) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 3, 12, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5532), new DateTime(2018, 5, 24, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5511) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 9, 7, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5584), new DateTime(2014, 2, 10, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5563) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 12, 16, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5632), new DateTime(2018, 9, 16, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5610) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 10, 29, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5731), new DateTime(2017, 11, 5, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5710) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5777), new DateTime(2013, 7, 6, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5756) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "PerformerId",
                table: "Tasks",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2009, 9, 10, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2525), new DateTime(2018, 3, 16, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2500) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2012, 6, 26, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5362), new DateTime(2021, 12, 5, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5325) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2008, 6, 10, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5417), new DateTime(2020, 9, 14, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5393) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2007, 8, 13, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5466), new DateTime(2019, 8, 19, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5443) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 5, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5605));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 6, 24, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(8590), new DateTime(2021, 7, 6, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(8629) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 15, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(9067));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 2, 20, 26, 48, 544, DateTimeKind.Local).AddTicks(9736));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 8, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8638));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8690));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 20, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8718));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 10, 31, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8908), new DateTime(2017, 7, 9, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8883) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2007, 2, 28, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1888), new DateTime(2013, 7, 3, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1851) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2006, 3, 28, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1940), new DateTime(2016, 1, 26, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1918) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2019, 10, 19, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2078), new DateTime(2016, 7, 10, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1966) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2010, 6, 27, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2128), new DateTime(2017, 9, 14, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2106) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2006, 10, 15, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2181), new DateTime(2018, 9, 14, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2158) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 12, 16, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2230), new DateTime(2016, 10, 9, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2207) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2006, 6, 17, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2279), new DateTime(2014, 11, 3, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2257) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2329), new DateTime(2017, 2, 12, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2307) });
        }
    }
}
