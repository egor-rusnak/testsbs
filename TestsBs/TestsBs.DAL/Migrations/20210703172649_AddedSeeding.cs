﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace TestsBs.DAL.Migrations
{
    public partial class AddedSeeding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 7, 2, 20, 26, 48, 544, DateTimeKind.Local).AddTicks(9736), "Bananas" },
                    { 2, new DateTime(2021, 7, 8, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8638), "Changes" },
                    { 3, new DateTime(2021, 6, 18, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8690), "Packages" },
                    { 4, new DateTime(2021, 6, 20, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8718), "asdas" }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 6, new DateTime(2006, 10, 15, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2181), "", "George", "Jakson", new DateTime(2018, 9, 14, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2158), null },
                    { 7, new DateTime(2009, 12, 16, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2230), "", "Annet", "Wonderson", new DateTime(2016, 10, 9, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2207), null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 4, 7, new DateTime(2007, 8, 13, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5466), new DateTime(2019, 8, 19, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5443), "Des4", "ProjName4", 3 });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 1, new DateTime(2011, 10, 31, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8908), "", "John", "Jonson", new DateTime(2017, 7, 9, 20, 26, 48, 547, DateTimeKind.Local).AddTicks(8883), 1 },
                    { 8, new DateTime(2006, 6, 17, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2279), "", "Marcus", "Jonson", new DateTime(2014, 11, 3, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2257), 1 },
                    { 4, new DateTime(2019, 10, 19, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2078), "", "Boris", "Apostolov", new DateTime(2016, 7, 10, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1966), 2 },
                    { 5, new DateTime(2010, 6, 27, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2128), "", "Grace", "Donalds", new DateTime(2017, 9, 14, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2106), 2 },
                    { 2, new DateTime(2007, 2, 28, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1888), "", "Marchel", "Davidson", new DateTime(2013, 7, 3, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1851), 3 },
                    { 3, new DateTime(2006, 3, 28, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1940), "", "Anchey", "Brownly", new DateTime(2016, 1, 26, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(1918), 3 },
                    { 9, new DateTime(1921, 7, 3, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2329), "", "Marcus", "Jonson", new DateTime(2017, 2, 12, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2307), 4 }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 1, 1, new DateTime(2009, 9, 10, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2525), new DateTime(2018, 3, 16, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(2500), "Des1", "ProjName1", 1 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 3, 8, new DateTime(2008, 6, 10, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5417), new DateTime(2020, 9, 14, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5393), "Des3", "ProjName3", 2 });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[] { 2, 5, new DateTime(2012, 6, 26, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5362), new DateTime(2021, 12, 5, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5325), "Des2", "ProjName2", 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { 1, new DateTime(2021, 7, 5, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(5605), "no", null, "task1", 1, 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { 2, new DateTime(2021, 6, 24, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(8590), "no", new DateTime(2021, 7, 6, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(8629), "task2", 1, 1 });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId" },
                values: new object[] { 3, new DateTime(2021, 7, 15, 20, 26, 48, 548, DateTimeKind.Local).AddTicks(9067), "no", null, "task3", 1, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
