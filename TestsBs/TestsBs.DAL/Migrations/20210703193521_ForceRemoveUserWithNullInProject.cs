﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace TestsBs.DAL.Migrations
{
    public partial class ForceRemoveUserWithNullInProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2019, 9, 28, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(699), new DateTime(2020, 11, 14, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(669) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2012, 4, 25, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(4038), new DateTime(2020, 8, 6, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(4001) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2009, 6, 25, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(4090), new DateTime(2022, 2, 16, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(4068) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2013, 8, 19, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(4137), new DateTime(2018, 5, 23, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(4117) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 22, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(4292));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 7, 14, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(7726), new DateTime(2021, 6, 20, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(7761) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 14, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(8217));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 6, 22, 35, 20, 744, DateTimeKind.Local).AddTicks(3182));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 2, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(5569));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 15, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(5633));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 21, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(5661));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 4, 9, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(5984), new DateTime(2015, 9, 6, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(5958) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2018, 9, 27, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(9890), new DateTime(2016, 10, 9, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(9841) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2004, 7, 15, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(9946), new DateTime(2016, 2, 6, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(9923) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2018, 9, 28, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(94), new DateTime(2014, 10, 24, 22, 35, 20, 747, DateTimeKind.Local).AddTicks(9975) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2004, 3, 28, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(245), new DateTime(2014, 11, 25, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(224) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2012, 7, 14, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(303), new DateTime(2019, 2, 5, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(279) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2019, 9, 13, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(352), new DateTime(2016, 12, 3, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(329) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 2, 26, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(402), new DateTime(2015, 1, 15, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(379) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(453), new DateTime(2013, 11, 15, 22, 35, 20, 748, DateTimeKind.Local).AddTicks(430) });

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2006, 5, 27, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1701), new DateTime(2019, 3, 17, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1676) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2005, 1, 14, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4716), new DateTime(2020, 11, 22, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4678) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2014, 10, 10, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4772), new DateTime(2022, 7, 10, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4746) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2017, 8, 12, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4824), new DateTime(2015, 5, 25, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4800) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 17, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 7, 1, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(8330), new DateTime(2021, 6, 20, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(8371) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(8981));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 17, 22, 33, 36, 842, DateTimeKind.Local).AddTicks(4518));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 5, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7669));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 16, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7725));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 7, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7753));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 8, 10, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7981), new DateTime(2019, 5, 1, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7955) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 1, 30, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1038), new DateTime(2013, 10, 23, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(998) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 2, 13, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1167), new DateTime(2020, 1, 23, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1141) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 10, 28, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1223), new DateTime(2014, 8, 13, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1198) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2004, 4, 18, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1279), new DateTime(2015, 2, 3, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1255) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 10, 10, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1337), new DateTime(2012, 12, 24, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1314) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 12, 26, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1389), new DateTime(2020, 2, 13, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1364) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2004, 8, 26, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1441), new DateTime(2018, 12, 2, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1417) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1493), new DateTime(2013, 6, 11, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1469) });

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id");
        }
    }
}
