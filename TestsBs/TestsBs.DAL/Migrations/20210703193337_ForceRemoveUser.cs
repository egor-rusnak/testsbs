﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace TestsBs.DAL.Migrations
{
    public partial class ForceRemoveUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2006, 5, 27, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1701), new DateTime(2019, 3, 17, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1676) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2005, 1, 14, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4716), new DateTime(2020, 11, 22, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4678) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2014, 10, 10, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4772), new DateTime(2022, 7, 10, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4746) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2017, 8, 12, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4824), new DateTime(2015, 5, 25, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4800) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 17, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(4982));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 7, 1, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(8330), new DateTime(2021, 6, 20, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(8371) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(8981));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 17, 22, 33, 36, 842, DateTimeKind.Local).AddTicks(4518));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 5, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7669));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 16, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7725));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 7, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7753));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 8, 10, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7981), new DateTime(2019, 5, 1, 22, 33, 36, 845, DateTimeKind.Local).AddTicks(7955) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 1, 30, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1038), new DateTime(2013, 10, 23, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(998) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 2, 13, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1167), new DateTime(2020, 1, 23, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1141) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 10, 28, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1223), new DateTime(2014, 8, 13, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1198) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2004, 4, 18, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1279), new DateTime(2015, 2, 3, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1255) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 10, 10, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1337), new DateTime(2012, 12, 24, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1314) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 12, 26, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1389), new DateTime(2020, 2, 13, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1364) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2004, 8, 26, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1441), new DateTime(2018, 12, 2, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1417) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1493), new DateTime(2013, 6, 11, 22, 33, 36, 846, DateTimeKind.Local).AddTicks(1469) });

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2005, 2, 18, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(2055), new DateTime(2015, 7, 19, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(2029) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2005, 1, 4, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4968), new DateTime(2020, 7, 3, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4931) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2015, 1, 19, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5018), new DateTime(2018, 6, 5, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4996) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2016, 11, 20, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5066), new DateTime(2018, 10, 21, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5044) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5209));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 7, 4, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8197), new DateTime(2021, 6, 28, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8231) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8662));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 17, 22, 28, 12, 422, DateTimeKind.Local).AddTicks(6646));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8275));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 6, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8327));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 4, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8352));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 10, 9, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8549), new DateTime(2016, 2, 29, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8526) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2010, 11, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1456), new DateTime(2015, 4, 9, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1420) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2018, 3, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1507), new DateTime(2019, 3, 30, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1486) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 2, 1, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1555), new DateTime(2012, 11, 7, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1533) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 8, 7, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1602), new DateTime(2014, 9, 26, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1580) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2020, 1, 29, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1655), new DateTime(2014, 10, 27, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1633) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 1, 30, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1704), new DateTime(2018, 11, 20, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1682) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 9, 25, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1808), new DateTime(2015, 9, 23, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1786) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1855), new DateTime(2013, 2, 24, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1832) });

            migrationBuilder.AddForeignKey(
                name: "FK_Projects_Users_AuthorId",
                table: "Projects",
                column: "AuthorId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
