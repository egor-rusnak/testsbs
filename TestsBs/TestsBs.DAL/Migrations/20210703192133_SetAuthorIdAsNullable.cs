﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace TestsBs.DAL.Migrations
{
    public partial class SetAuthorIdAsNullable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "Projects",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2018, 2, 18, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9799), new DateTime(2019, 3, 26, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9776) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2014, 2, 21, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2633), new DateTime(2019, 5, 10, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2597) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2004, 10, 11, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2685), new DateTime(2018, 2, 3, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2663) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2008, 11, 13, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2735), new DateTime(2021, 9, 5, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2712) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 19, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2874));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 6, 20, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(6033), new DateTime(2021, 7, 3, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(6069) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 5, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(6494));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 16, 22, 21, 33, 414, DateTimeKind.Local).AddTicks(6769));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 21, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(5965));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 27, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6016));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 30, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6043));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 4, 24, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6226), new DateTime(2012, 11, 2, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6201) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2011, 8, 30, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9226), new DateTime(2016, 12, 7, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9189) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2014, 10, 19, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9333), new DateTime(2019, 10, 31, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9255) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 2, 21, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9382), new DateTime(2013, 5, 24, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9359) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2019, 6, 25, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9430), new DateTime(2016, 9, 18, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9408) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2016, 8, 21, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9480), new DateTime(2015, 2, 24, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9459) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2017, 11, 25, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9527), new DateTime(2013, 5, 6, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9506) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2012, 7, 17, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9575), new DateTime(2017, 6, 10, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9552) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9623), new DateTime(2019, 5, 9, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9601) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "Projects",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2006, 6, 29, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5966), new DateTime(2016, 4, 10, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5942) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2012, 7, 30, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8735), new DateTime(2015, 4, 8, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8700) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2014, 11, 11, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8785), new DateTime(2020, 4, 26, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8764) });

            migrationBuilder.UpdateData(
                table: "Projects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CreatedAt", "Deadline" },
                values: new object[] { new DateTime(2010, 4, 15, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8834), new DateTime(2020, 12, 9, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8811) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(8976));

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CreatedAt", "FinishedAt" },
                values: new object[] { new DateTime(2021, 7, 14, 22, 19, 2, 175, DateTimeKind.Local).AddTicks(2119), new DateTime(2021, 7, 7, 22, 19, 2, 175, DateTimeKind.Local).AddTicks(2156) });

            migrationBuilder.UpdateData(
                table: "Tasks",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 30, 22, 19, 2, 175, DateTimeKind.Local).AddTicks(2585));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 1,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 3, 22, 19, 2, 171, DateTimeKind.Local).AddTicks(3214));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 2,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 29, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2152));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 3,
                column: "CreatedAt",
                value: new DateTime(2021, 6, 18, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2202));

            migrationBuilder.UpdateData(
                table: "Teams",
                keyColumn: "Id",
                keyValue: 4,
                column: "CreatedAt",
                value: new DateTime(2021, 7, 5, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2228));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2010, 1, 8, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2411), new DateTime(2016, 7, 10, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(2387) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2007, 9, 12, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5385), new DateTime(2013, 2, 6, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5350) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2008, 10, 6, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5437), new DateTime(2017, 12, 14, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5415) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 6, 13, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5485), new DateTime(2012, 1, 11, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5463) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 3, 12, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5532), new DateTime(2018, 5, 24, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5511) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2005, 9, 7, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5584), new DateTime(2014, 2, 10, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5563) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2013, 12, 16, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5632), new DateTime(2018, 9, 16, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5610) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(2009, 10, 29, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5731), new DateTime(2017, 11, 5, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5710) });

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "BirthDay", "RegisteredAt" },
                values: new object[] { new DateTime(1921, 7, 3, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5777), new DateTime(2013, 7, 6, 22, 19, 2, 174, DateTimeKind.Local).AddTicks(5756) });
        }
    }
}
