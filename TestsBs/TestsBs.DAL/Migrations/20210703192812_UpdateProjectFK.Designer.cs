﻿// <auto-generated />
using System;
using TestsBs.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace TestsBs.DAL.Migrations
{
    [DbContext(typeof(ProjectDbContext))]
    [Migration("20210703192812_UpdateProjectFK")]
    partial class UpdateProjectFK
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AuthorId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Deadline")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasMaxLength(500)
                        .HasColumnType("nvarchar(500)");

                    b.Property<string>("Name")
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("TeamId");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorId = 1,
                            CreatedAt = new DateTime(2005, 2, 18, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(2055),
                            Deadline = new DateTime(2015, 7, 19, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(2029),
                            Description = "Des1",
                            Name = "ProjName1",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            AuthorId = 5,
                            CreatedAt = new DateTime(2005, 1, 4, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4968),
                            Deadline = new DateTime(2020, 7, 3, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4931),
                            Description = "Des2",
                            Name = "ProjName2",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 3,
                            AuthorId = 8,
                            CreatedAt = new DateTime(2015, 1, 19, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5018),
                            Deadline = new DateTime(2018, 6, 5, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(4996),
                            Description = "Des3",
                            Name = "ProjName3",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 4,
                            AuthorId = 7,
                            CreatedAt = new DateTime(2016, 11, 20, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5066),
                            Deadline = new DateTime(2018, 10, 21, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5044),
                            Description = "Des4",
                            Name = "ProjName4",
                            TeamId = 3
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Task", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasMaxLength(500)
                        .HasColumnType("nvarchar(500)");

                    b.Property<DateTime?>("FinishedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.Property<int?>("PerformerId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("PerformerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 6, 18, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(5209),
                            Description = "no",
                            Name = "task1",
                            PerformerId = 1,
                            ProjectId = 1
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 7, 4, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8197),
                            Description = "no",
                            FinishedAt = new DateTime(2021, 6, 28, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8231),
                            Name = "task2",
                            PerformerId = 1,
                            ProjectId = 1
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 7, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(8662),
                            Description = "no",
                            Name = "task3",
                            PerformerId = 1,
                            ProjectId = 1
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 7, 17, 22, 28, 12, 422, DateTimeKind.Local).AddTicks(6646),
                            Name = "Bananas"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 6, 18, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8275),
                            Name = "Changes"
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 7, 6, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8327),
                            Name = "Packages"
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2021, 7, 4, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8352),
                            Name = "asdas"
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BirthDay")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("FirstName")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("LastName")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<DateTime>("RegisteredAt")
                        .HasColumnType("datetime2");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            BirthDay = new DateTime(2014, 10, 9, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8549),
                            Email = "",
                            FirstName = "John",
                            LastName = "Jonson",
                            RegisteredAt = new DateTime(2016, 2, 29, 22, 28, 12, 425, DateTimeKind.Local).AddTicks(8526),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            BirthDay = new DateTime(2010, 11, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1456),
                            Email = "",
                            FirstName = "Marchel",
                            LastName = "Davidson",
                            RegisteredAt = new DateTime(2015, 4, 9, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1420),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 3,
                            BirthDay = new DateTime(2018, 3, 16, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1507),
                            Email = "",
                            FirstName = "Anchey",
                            LastName = "Brownly",
                            RegisteredAt = new DateTime(2019, 3, 30, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1486),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 4,
                            BirthDay = new DateTime(2011, 2, 1, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1555),
                            Email = "",
                            FirstName = "Boris",
                            LastName = "Apostolov",
                            RegisteredAt = new DateTime(2012, 11, 7, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1533),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 5,
                            BirthDay = new DateTime(2014, 8, 7, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1602),
                            Email = "",
                            FirstName = "Grace",
                            LastName = "Donalds",
                            RegisteredAt = new DateTime(2014, 9, 26, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1580),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 6,
                            BirthDay = new DateTime(2020, 1, 29, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1655),
                            Email = "",
                            FirstName = "George",
                            LastName = "Jakson",
                            RegisteredAt = new DateTime(2014, 10, 27, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1633)
                        },
                        new
                        {
                            Id = 7,
                            BirthDay = new DateTime(2013, 1, 30, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1704),
                            Email = "",
                            FirstName = "Annet",
                            LastName = "Wonderson",
                            RegisteredAt = new DateTime(2018, 11, 20, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1682)
                        },
                        new
                        {
                            Id = 8,
                            BirthDay = new DateTime(2005, 9, 25, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1808),
                            Email = "",
                            FirstName = "Marcus",
                            LastName = "Jonson",
                            RegisteredAt = new DateTime(2015, 9, 23, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1786),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 9,
                            BirthDay = new DateTime(1921, 7, 3, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1855),
                            Email = "",
                            FirstName = "Marcus",
                            LastName = "Jonson",
                            RegisteredAt = new DateTime(2013, 2, 24, 22, 28, 12, 426, DateTimeKind.Local).AddTicks(1832),
                            TeamId = 4
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Project", b =>
                {
                    b.HasOne("EntityFrameworkBs.DAL.Entities.User", "Author")
                        .WithMany()
                        .HasForeignKey("AuthorId");

                    b.HasOne("EntityFrameworkBs.DAL.Entities.Team", "Team")
                        .WithMany()
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Task", b =>
                {
                    b.HasOne("EntityFrameworkBs.DAL.Entities.User", "Performer")
                        .WithMany("Tasks")
                        .HasForeignKey("PerformerId")
                        .OnDelete(DeleteBehavior.NoAction);

                    b.HasOne("EntityFrameworkBs.DAL.Entities.Project", null)
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Performer");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.User", b =>
                {
                    b.HasOne("EntityFrameworkBs.DAL.Entities.Team", "Team")
                        .WithMany("Members")
                        .HasForeignKey("TeamId");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Project", b =>
                {
                    b.Navigation("Tasks");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Team", b =>
                {
                    b.Navigation("Members");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.User", b =>
                {
                    b.Navigation("Tasks");
                });
#pragma warning restore 612, 618
        }
    }
}
