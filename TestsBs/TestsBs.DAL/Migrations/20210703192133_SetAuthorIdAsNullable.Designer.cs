﻿// <auto-generated />
using System;
using TestsBs.DAL.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace TestsBs.DAL.Migrations
{
    [DbContext(typeof(ProjectDbContext))]
    [Migration("20210703192133_SetAuthorIdAsNullable")]
    partial class SetAuthorIdAsNullable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.7")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Project", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int?>("AuthorId")
                        .HasColumnType("int");

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<DateTime>("Deadline")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasMaxLength(500)
                        .HasColumnType("nvarchar(500)");

                    b.Property<string>("Name")
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.Property<int>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("AuthorId");

                    b.HasIndex("TeamId");

                    b.ToTable("Projects");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            AuthorId = 1,
                            CreatedAt = new DateTime(2018, 2, 18, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9799),
                            Deadline = new DateTime(2019, 3, 26, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9776),
                            Description = "Des1",
                            Name = "ProjName1",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            AuthorId = 5,
                            CreatedAt = new DateTime(2014, 2, 21, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2633),
                            Deadline = new DateTime(2019, 5, 10, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2597),
                            Description = "Des2",
                            Name = "ProjName2",
                            TeamId = 1
                        },
                        new
                        {
                            Id = 3,
                            AuthorId = 8,
                            CreatedAt = new DateTime(2004, 10, 11, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2685),
                            Deadline = new DateTime(2018, 2, 3, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2663),
                            Description = "Des3",
                            Name = "ProjName3",
                            TeamId = 2
                        },
                        new
                        {
                            Id = 4,
                            AuthorId = 7,
                            CreatedAt = new DateTime(2008, 11, 13, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2735),
                            Deadline = new DateTime(2021, 9, 5, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2712),
                            Description = "Des4",
                            Name = "ProjName4",
                            TeamId = 3
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Task", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Description")
                        .HasMaxLength(500)
                        .HasColumnType("nvarchar(500)");

                    b.Property<DateTime?>("FinishedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.Property<int?>("PerformerId")
                        .HasColumnType("int");

                    b.Property<int>("ProjectId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("PerformerId");

                    b.HasIndex("ProjectId");

                    b.ToTable("Tasks");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 6, 19, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(2874),
                            Description = "no",
                            Name = "task1",
                            PerformerId = 1,
                            ProjectId = 1
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 6, 20, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(6033),
                            Description = "no",
                            FinishedAt = new DateTime(2021, 7, 3, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(6069),
                            Name = "task2",
                            PerformerId = 1,
                            ProjectId = 1
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 7, 5, 22, 21, 33, 418, DateTimeKind.Local).AddTicks(6494),
                            Description = "no",
                            Name = "task3",
                            PerformerId = 1,
                            ProjectId = 1
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("CreatedAt")
                        .HasColumnType("datetime2");

                    b.Property<string>("Name")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("Id");

                    b.ToTable("Teams");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            CreatedAt = new DateTime(2021, 7, 16, 22, 21, 33, 414, DateTimeKind.Local).AddTicks(6769),
                            Name = "Bananas"
                        },
                        new
                        {
                            Id = 2,
                            CreatedAt = new DateTime(2021, 6, 21, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(5965),
                            Name = "Changes"
                        },
                        new
                        {
                            Id = 3,
                            CreatedAt = new DateTime(2021, 6, 27, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6016),
                            Name = "Packages"
                        },
                        new
                        {
                            Id = 4,
                            CreatedAt = new DateTime(2021, 6, 30, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6043),
                            Name = "asdas"
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<DateTime>("BirthDay")
                        .HasColumnType("datetime2");

                    b.Property<string>("Email")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("FirstName")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("LastName")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<DateTime>("RegisteredAt")
                        .HasColumnType("datetime2");

                    b.Property<int?>("TeamId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("TeamId");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            Id = 1,
                            BirthDay = new DateTime(2011, 4, 24, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6226),
                            Email = "",
                            FirstName = "John",
                            LastName = "Jonson",
                            RegisteredAt = new DateTime(2012, 11, 2, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(6201),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 2,
                            BirthDay = new DateTime(2011, 8, 30, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9226),
                            Email = "",
                            FirstName = "Marchel",
                            LastName = "Davidson",
                            RegisteredAt = new DateTime(2016, 12, 7, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9189),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 3,
                            BirthDay = new DateTime(2014, 10, 19, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9333),
                            Email = "",
                            FirstName = "Anchey",
                            LastName = "Brownly",
                            RegisteredAt = new DateTime(2019, 10, 31, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9255),
                            TeamId = 3
                        },
                        new
                        {
                            Id = 4,
                            BirthDay = new DateTime(2005, 2, 21, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9382),
                            Email = "",
                            FirstName = "Boris",
                            LastName = "Apostolov",
                            RegisteredAt = new DateTime(2013, 5, 24, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9359),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 5,
                            BirthDay = new DateTime(2019, 6, 25, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9430),
                            Email = "",
                            FirstName = "Grace",
                            LastName = "Donalds",
                            RegisteredAt = new DateTime(2016, 9, 18, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9408),
                            TeamId = 2
                        },
                        new
                        {
                            Id = 6,
                            BirthDay = new DateTime(2016, 8, 21, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9480),
                            Email = "",
                            FirstName = "George",
                            LastName = "Jakson",
                            RegisteredAt = new DateTime(2015, 2, 24, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9459)
                        },
                        new
                        {
                            Id = 7,
                            BirthDay = new DateTime(2017, 11, 25, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9527),
                            Email = "",
                            FirstName = "Annet",
                            LastName = "Wonderson",
                            RegisteredAt = new DateTime(2013, 5, 6, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9506)
                        },
                        new
                        {
                            Id = 8,
                            BirthDay = new DateTime(2012, 7, 17, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9575),
                            Email = "",
                            FirstName = "Marcus",
                            LastName = "Jonson",
                            RegisteredAt = new DateTime(2017, 6, 10, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9552),
                            TeamId = 1
                        },
                        new
                        {
                            Id = 9,
                            BirthDay = new DateTime(1921, 7, 3, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9623),
                            Email = "",
                            FirstName = "Marcus",
                            LastName = "Jonson",
                            RegisteredAt = new DateTime(2019, 5, 9, 22, 21, 33, 417, DateTimeKind.Local).AddTicks(9601),
                            TeamId = 4
                        });
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Project", b =>
                {
                    b.HasOne("EntityFrameworkBs.DAL.Entities.User", "Author")
                        .WithMany()
                        .HasForeignKey("AuthorId")
                        .OnDelete(DeleteBehavior.NoAction);

                    b.HasOne("EntityFrameworkBs.DAL.Entities.Team", "Team")
                        .WithMany()
                        .HasForeignKey("TeamId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Author");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Task", b =>
                {
                    b.HasOne("EntityFrameworkBs.DAL.Entities.User", "Performer")
                        .WithMany("Tasks")
                        .HasForeignKey("PerformerId")
                        .OnDelete(DeleteBehavior.NoAction);

                    b.HasOne("EntityFrameworkBs.DAL.Entities.Project", null)
                        .WithMany("Tasks")
                        .HasForeignKey("ProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Performer");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.User", b =>
                {
                    b.HasOne("EntityFrameworkBs.DAL.Entities.Team", "Team")
                        .WithMany("Members")
                        .HasForeignKey("TeamId");

                    b.Navigation("Team");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Project", b =>
                {
                    b.Navigation("Tasks");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.Team", b =>
                {
                    b.Navigation("Members");
                });

            modelBuilder.Entity("EntityFrameworkBs.DAL.Entities.User", b =>
                {
                    b.Navigation("Tasks");
                });
#pragma warning restore 612, 618
        }
    }
}
