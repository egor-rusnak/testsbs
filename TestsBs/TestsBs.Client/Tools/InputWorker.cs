﻿using System;

namespace TestsBs.UI.Tools
{
    public static class InputWorker
    {
        public static string GetString(string title)
        {
            Console.Write(title);
            return Console.ReadLine();
        }

        public static int GetNumber(string title)
        {
            Console.WriteLine("To exit write exit word");
            Console.Write(title);
            while (true)
            {
                var result = Console.ReadLine();
                if (result == "exit") throw new ArgumentException("Not written data");
                if (int.TryParse(result, out int number))
                {
                    return number;
                }
                else continue;
            }
        }
        public static DateTime GetDate(string title)
        {
            Console.Write(title);
            DateTime.TryParse(Console.ReadLine(), out DateTime result);
            return result;
        }

    }
}
