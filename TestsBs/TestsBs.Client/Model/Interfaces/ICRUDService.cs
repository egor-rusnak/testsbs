﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TestsBs.Common.DTOs.Abstraction;

namespace TestsBs.UI.Model.Interfaces
{
    public interface ICRUDService<T> where T : BaseEntityDto
    {
        Task<T> Create(T entity);
        Task<bool> Update(T entity);
        Task<IEnumerable<T>> GetAll();
        Task<bool> Delete(int id);
    }
}
