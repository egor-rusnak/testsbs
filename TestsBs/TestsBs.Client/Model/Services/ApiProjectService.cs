﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using TestsBs.Common.DTOs;
using TestsBs.Common.DTOs.Project;
using TestsBs.Common.DTOs.User;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.UI.Model.Services
{
    public class ApiProjectService : IProjectService
    {
        private readonly HttpClient _client;
        public ApiProjectService(Uri apiAdress)
        {
            _client = new HttpClient()
            {
                BaseAddress = apiAdress
            };
        }

        public void Dispose()
        {
            _client.Dispose();
        }

        public async Task<IEnumerable<KeyValuePair<UserDto, List<TaskDto>>>> ExecuteQueryFive()
        {
            return await _client.GetFromJsonAsync<List<KeyValuePair<UserDto, List<TaskDto>>>>("api/Queries/Fifth");
        }

        public async Task<IEnumerable<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>> ExecuteQueryFour()
        {
            return await _client.GetFromJsonAsync<List<KeyValuePair<KeyValuePair<int, string>, IEnumerable<UserDto>>>>("api/Queries/Fourth");
        }

        public async Task<Dictionary<ProjectDto, int>> ExecuteQueryOne(int userId)
        {
            return new Dictionary<ProjectDto, int>(await _client.GetFromJsonAsync<List<KeyValuePair<ProjectDto, int>>>($"api/Queries/First/{userId}"));
        }

        public async Task<IEnumerable<ProjectInfoDto>> ExecuteQuerySeven()
        {
            return await _client.GetFromJsonAsync<List<ProjectInfoDto>>("api/Queries/Seventh");
        }

        public async Task<UserInfoDto> ExecuteQuerySix(int userId)
        {
            return await _client.GetFromJsonAsync<UserInfoDto>($"api/Queries/Sixth/{userId}");
        }

        public async Task<IEnumerable<KeyValuePair<int, string>>> ExecuteQueryThree(int userId)
        {
            return await _client.GetFromJsonAsync<List<KeyValuePair<int, string>>>($"api/Queries/Third/{userId}");
        }

        public async Task<IEnumerable<TaskDto>> ExecuteQueryTwo(int userId)
        {
            return await _client.GetFromJsonAsync<List<TaskDto>>($"api/Queries/Second/{userId}");
        }

        public async Task<ProjectDto> CreateProject(ProjectDto project)
        {
            var response = await _client.PostAsJsonAsync("api/Projects/", project);
            return await response.Content.ReadFromJsonAsync<ProjectDto>();
        }
        public async Task<bool> UpdateProject(ProjectDto project)
        {
            var result = await _client.PutAsJsonAsync("api/Projects/", project);
            if (result.IsSuccessStatusCode)
                return true;
            else
                return false;
        }
        public async Task<IEnumerable<ProjectDto>> GetProjects()
        {
            return await _client.GetFromJsonAsync<List<ProjectDto>>("api/Projects/");
        }
        public async Task<bool> DeleteProject(int id)
        {
            var response = await _client.DeleteAsync("api/Projects/" + id);

            if (response.IsSuccessStatusCode)
                return true;
            else
                return false;
        }
    }
}
