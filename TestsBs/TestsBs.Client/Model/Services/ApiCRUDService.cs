﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using TestsBs.Common.DTOs.Abstraction;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.UI.Model.Services
{
    public class ApiCRUDService<T> : ICRUDService<T> where T : BaseEntityDto
    {

        private string _prefixApiString;

        private readonly HttpClient _client;
        public ApiCRUDService(Uri apiAdress, string prefixApiString)
        {
            _client = new HttpClient()
            {
                BaseAddress = apiAdress
            };
            _prefixApiString = prefixApiString;
        }

        public async Task<T> Create(T entity)
        {
            var response = await _client.PostAsJsonAsync(_prefixApiString, entity);
            if (response.IsSuccessStatusCode)
                return await response.Content.ReadFromJsonAsync<T>();
            else
            {
                throw new Exception(await response.Content.ReadAsStringAsync());
            }
        }

        public async Task<bool> Update(T entity)
        {
            var response = await _client.PutAsJsonAsync(_prefixApiString, entity);
            if (response.IsSuccessStatusCode)
                return true;
            else
            {
                Console.WriteLine("Error: " + await response.Content.ReadAsStringAsync());
                return false;
            }
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            return await _client.GetFromJsonAsync<List<T>>(_prefixApiString);
        }

        public async Task<bool> Delete(int id)
        {
            var response = await _client.DeleteAsync(_prefixApiString + id);

            if (response.IsSuccessStatusCode)
                return true;
            else
            {
                Console.WriteLine("Error: " + await response.Content.ReadAsStringAsync());
                return false;
            }
        }
    }
}
