﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using TestsBs.Application.Commands;
using TestsBs.Common.DTOs;
using TestsBs.Common.DTOs.Project;
using TestsBs.Common.DTOs.User;
using TestsBs.UI.Commands.WorkWithData;
using TestsBs.UI.Model.Interfaces;
using TestsBs.UI.Model.Services;

namespace TestsBs
{
    class Program
    {
        private static List<ICommand> commands = new List<ICommand>();

        static async Task Main(string[] args)
        {
            CultureInfo.CurrentCulture = CultureInfo.GetCultureInfo("ua");
            var uri = new Uri("https://localhost:44380/");
            var projectService = new ApiProjectService(uri);
            var projectCrudService = new ApiCRUDService<ProjectDto>(uri, "api/Projects/");
            var taskCrudService = new ApiCRUDService<TaskDto>(uri, "api/Tasks/");
            var userCrudService = new ApiCRUDService<UserDto>(uri, "api/Users/");
            var teamCrudService = new ApiCRUDService<TeamDto>(uri, "api/Teams/");

            SetCommands(projectService, projectCrudService, taskCrudService, teamCrudService, userCrudService);

            await RunApplication();
        }

        static async Task RunApplication()
        {
            Console.WriteLine("Console interface for BS task `EntityFramework`");
            Console.WriteLine("To show menu, write a 0");
            ShowMenu();
            while (true)
            {
                Console.Write("Input a command (0-show menu): ");
                var input = Console.ReadLine();

                if (int.TryParse(input, out int command))
                {
                    if (command > 0 && command <= commands.Count)
                    {
                        try
                        {
                            await commands[command - 1].Execute();
                            Console.WriteLine();
                            continue;
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Unhandled error: " + ex.Message);
                            continue;
                        }
                    }
                    else if (command == 0)
                    {
                        ShowMenu();
                        continue;
                    }
                    else if (command == commands.Count + 1) break;
                }

                Console.WriteLine("Wrong Input");
            }
            Console.WriteLine("Good Bye!");
        }

        static void ShowMenu()
        {
            Console.WriteLine("0-Show menu");
            int index = 1;
            foreach (var command in commands)
            {
                Console.WriteLine(index + "-" + command.Description);
                index++;
            }
            Console.WriteLine((index) + "-Exit");
        }

        private static void SetCommands(
            IProjectService projects,
            ICRUDService<ProjectDto> projectCrud,
            ICRUDService<TaskDto> tasksCrud,
            ICRUDService<TeamDto> teamsCrud,
            ICRUDService<UserDto> usersCrud)
        {
            commands.Add(new ExecuteFirstQuery(projects));
            commands.Add(new ExecuteSecondQuery(projects));
            commands.Add(new ExecuteThirdQuery(projects));
            commands.Add(new ExecuteFourthQuery(projects));
            commands.Add(new ExecuteFifthQuery(projects));
            commands.Add(new ExecuteSixthQuery(projects));
            commands.Add(new ExecuteSeventhQuery(projects));
            commands.Add(new WorkWithProjectsCRUD(projectCrud));
            commands.Add(new WorkWithTasksCRUD(tasksCrud));
            commands.Add(new WorkWithTeamsCRUD(teamsCrud));
            commands.Add(new WorkWithUsersCRUD(usersCrud));
        }
    }
}
