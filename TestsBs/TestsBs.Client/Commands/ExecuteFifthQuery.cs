﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.Application.Commands
{
    public class ExecuteFifthQuery : ICommand
    {
        private readonly IProjectService _projectService;
        public ExecuteFifthQuery(IProjectService service)
        {
            _projectService = service;
        }
        public string Description { get; set; } = "Execute query number 5";
        public async Task Execute()
        {
            Console.WriteLine("Users ordered by first name: ");
            var result = await _projectService.ExecuteQueryFive();
            foreach (var elem in result)
            {
                Console.WriteLine($"User [{elem.Key}] with sorted by name length tasks:");
                foreach (var task in elem.Value)
                {
                    Console.WriteLine("\t" + task);
                }
                if (elem.Value.Count() == 0) Console.WriteLine("\tBLANK");
            }
            if (result.Count() == 0) Console.WriteLine("BLANK");
        }
    }
}
