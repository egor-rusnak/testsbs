﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.Application.Commands
{
    public class ExecuteFirstQuery : ICommand
    {
        private readonly IProjectService _projectService;
        public ExecuteFirstQuery(IProjectService service)
        {
            _projectService = service;
        }
        public string Description { get; set; } = "Execute query number 1";
        public async Task Execute()
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine("Result: ");
                var result = await _projectService.ExecuteQueryOne(input);
                foreach (var elem in result)
                {
                    Console.WriteLine($"Project id: {elem.Key.Id} and tasks count: {elem.Value}");
                }
                if (result.Count() == 0) Console.WriteLine("BLANK");
            }
        }
    }
}
