﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.Application.Commands
{
    public class ExecuteThirdQuery : ICommand
    {
        private readonly IProjectService _projectService;
        public ExecuteThirdQuery(IProjectService service)
        {
            _projectService = service;
        }

        public string Description { get; set; } = "Execute query number 3";
        public async Task Execute()
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Finished Tasks in {DateTime.Now.Year} from user {input}: ");
                var result = await _projectService.ExecuteQueryThree(input);
                foreach (var elem in result)
                {
                    Console.WriteLine($"Task id: {elem.Key}, Name: {elem.Value}");
                }
                if (result.Count() == 0) Console.WriteLine("BLANK");
            }
        }
    }
}
