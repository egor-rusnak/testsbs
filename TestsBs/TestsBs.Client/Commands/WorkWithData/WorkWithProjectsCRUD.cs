﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.Common.DTOs.Project;
using TestsBs.UI.Model.Interfaces;
using static TestsBs.UI.Tools.InputWorker;

namespace TestsBs.UI.Commands.WorkWithData
{
    public class WorkWithProjectsCRUD : WorkWithDataCRUD
    {
        private readonly ICRUDService<ProjectDto> _service;
        public WorkWithProjectsCRUD(ICRUDService<ProjectDto> service)
        {
            _service = service;
            Description = "Projects";
        }
        public override async Task Create()
        {
            var project = new ProjectDto()
            {
                CreatedAt = DateTime.Now,
                Name = GetString("Enter project name: "),
                Description = GetString("Enter project Description: "),
                Deadline = GetDate("Enter Deadline date: "),
                AuthorId = GetNumber("Input a author id: "),
                TeamId = GetNumber("Input team id: ")
            };
            var result = await _service.Create(project);

            Console.WriteLine("=====\n" + result.Id + "-" + result.Name + " was created..\n=====");
        }

        public override async Task Delete()
        {
            if (await _service.Delete(GetNumber("Input project id to delete: ")))
                Console.WriteLine("Success removing project!");
            else
                Console.WriteLine("Error removing project!");
        }

        public override async Task Read()
        {
            var projects = await _service.GetAll();
            Console.WriteLine("Projects List: ");
            foreach (var project in projects)
            {
                Console.WriteLine(project.Id + "-" + project.Name);
            }
        }

        public override async Task Update()
        {
            int id = GetNumber("Input project id: ");

            var project = (await _service.GetAll()).FirstOrDefault(p => p.Id == id);
            if (project == null)
            {
                Console.WriteLine("No project with this id");
                return;
            }

            project.Name = GetString($"Enter new project name (old: {project.Name}): ");
            project.Description = GetString($"Enter new project Description (old: {project.Description}): ");
            project.Deadline = GetDate($"Enter new Deadline date (old: {project.Deadline}): ");
            project.AuthorId = GetNumber($"Input a new author id (old: {project.AuthorId}): ");
            project.TeamId = GetNumber($"Input new team id (old: {project.TeamId}): ");

            if (await _service.Update(project))
                Console.WriteLine("Success with update project!");
            else
                Console.WriteLine("Error updating project!");
        }
    }
}
