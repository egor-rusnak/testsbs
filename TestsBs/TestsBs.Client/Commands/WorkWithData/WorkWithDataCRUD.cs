﻿using System;
using System.Threading.Tasks;
using TestsBs.Application.Commands;

namespace TestsBs.UI.Commands
{
    public abstract class WorkWithDataCRUD : ICommand
    {
        public string Description { get; set; }
        public async Task Execute()
        {
            Console.WriteLine(Description);
            Console.WriteLine("1-Create");
            Console.WriteLine("2-Update");
            Console.WriteLine("3-Read");
            Console.WriteLine("4-Delete");
            Console.WriteLine("5-Exit");
            while (true)
            {
                Console.Write("Input:");
                var result = Console.ReadLine();
                if (result == "1")
                    await Create();
                else if (result == "2")
                    await Update();
                else if (result == "3")
                    await Read();
                else if (result == "4")
                    await Delete();
                else if (result == "5")
                    break;
            }
        }

        public abstract Task Create();
        public abstract Task Update();
        public abstract Task Delete();
        public abstract Task Read();

    }
}
