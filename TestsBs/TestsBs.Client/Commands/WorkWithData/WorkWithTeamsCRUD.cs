﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.Common.DTOs;
using TestsBs.UI.Model.Interfaces;
using static TestsBs.UI.Tools.InputWorker;

namespace TestsBs.UI.Commands.WorkWithData
{
    public class WorkWithTeamsCRUD : WorkWithDataCRUD
    {
        private readonly ICRUDService<TeamDto> _service;
        public WorkWithTeamsCRUD(ICRUDService<TeamDto> service)
        {
            _service = service;
            Description = "Teams";
        }
        public override async Task Create()
        {
            var team = new TeamDto()
            {
                CreatedAt = DateTime.Now,
                Name = GetString("Enter team name: ")
            };

            var result = await _service.Create(team);
            if (result != null)
                Console.WriteLine("=====\n" + result.Id + "-" + result.Name + " was created..\n=====");
            else
                Console.WriteLine("Error creating team!");
        }

        public override async Task Delete()
        {
            if (await _service.Delete(GetNumber("Input team id to delete: ")))
                Console.WriteLine("Success removing team!");
            else
                Console.WriteLine("Error removing team!");
        }

        public override async Task Read()
        {
            var teams = await _service.GetAll();
            Console.WriteLine("Teams List:");
            foreach (var team in teams)
            {
                Console.WriteLine(team.Id + "-" + team.Name);
            }
        }

        public override async Task Update()
        {
            int id = GetNumber("Input team id: ");

            var team = (await _service.GetAll()).FirstOrDefault(p => p.Id == id);
            if (team == null)
            {
                Console.WriteLine("No team with this id");
                return;
            }
            team.Name = GetString($"Enter new team name (old: {team.Name}): ");

            if (await _service.Update(team))
                Console.WriteLine("Success with update team!");
            else
                Console.WriteLine("Error updating team!");
        }
    }
}
