﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.Common.DTOs;
using TestsBs.UI.Model.Interfaces;
using static TestsBs.UI.Tools.InputWorker;

namespace TestsBs.UI.Commands.WorkWithData
{
    public class WorkWithTasksCRUD : WorkWithDataCRUD
    {
        private readonly ICRUDService<TaskDto> _service;
        public WorkWithTasksCRUD(ICRUDService<TaskDto> service)
        {
            _service = service;
            Description = "Tasks";
        }
        public override async Task Create()
        {
            var task = new TaskDto()
            {
                CreatedAt = DateTime.Now,
                Name = GetString("Enter task name: "),
                Description = GetString("Enter task Description: "),
                FinishedAt = GetDate("Enter Finished At date: "),
                PerformerId = GetNumber("Enter Performer Id: "),
                ProjectId = GetNumber("Enter Project Id for task: ")
            };

            var result = await _service.Create(task);
            if (result != null)
                Console.WriteLine("=====\n" + result.Id + " - " + result.Name + " was created..Э\n=====");
            else
                Console.WriteLine("Error creating Task!");
        }

        public override async Task Delete()
        {
            if (await _service.Delete(GetNumber("Input task id to delete: ")))
                Console.WriteLine("Success removing task!");
            else
                Console.WriteLine("Error removing task!");
        }

        public override async Task Read()
        {
            var tasks = await _service.GetAll();
            Console.WriteLine("Tasks List:");
            foreach (var task in tasks)
            {
                Console.WriteLine(task.Id + "-" + task.Name);
            }
        }

        public override async Task Update()
        {
            int id = GetNumber("Input task id: ");

            var task = (await _service.GetAll()).FirstOrDefault(p => p.Id == id);
            if (task == null)
            {
                Console.WriteLine("No task with this id");
                return;
            }

            task.Name = GetString($"Enter new task name (old: {task.Name}): ");
            task.Description = GetString($"Enter new task Description (old: {task.Description}): ");
            task.FinishedAt = GetDate($"Enter new FinishedAt date (old: {task.FinishedAt}): ");
            task.PerformerId = GetNumber($"Input a new performer id (old: {task.PerformerId}): ");
            task.ProjectId = GetNumber($"Input new project id (old: {task.ProjectId}): ");

            if (await _service.Update(task))
                Console.WriteLine("Success with update task!");
            else
                Console.WriteLine("Error updating task!");
        }
    }
}
