﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.Common.DTOs.User;
using TestsBs.UI.Model.Interfaces;
using static TestsBs.UI.Tools.InputWorker;

namespace TestsBs.UI.Commands.WorkWithData
{
    public class WorkWithUsersCRUD : WorkWithDataCRUD
    {
        private readonly ICRUDService<UserDto> _service;
        public WorkWithUsersCRUD(ICRUDService<UserDto> service)
        {
            _service = service;
            Description = "Users";
        }
        public override async Task Create()
        {
            var user = new UserDto()
            {
                BirthDay = GetDate("Enter user BirthDay date: "),
                FirstName = GetString("Enter user first name: "),
                LastName = GetString("Enter user last name: "),
                Email = GetString("Enter user email: "),
                RegisteredAt = DateTime.Now,
                TeamId = GetNumber("Enter Team Id for user: ")
            };

            var result = await _service.Create(user);
            if (result != null)
                Console.WriteLine("=====\n" + result.Id + "-" + result.FirstName + " " + result.LastName + " was created..\n=====");
            else
                Console.WriteLine("Error creating User!");
        }

        public override async Task Delete()
        {
            if (await _service.Delete(GetNumber("Input User id to delete: ")))
                Console.WriteLine("Success removing User!");
            else
                Console.WriteLine("Error removing User!");
        }

        public override async Task Read()
        {
            var users = await _service.GetAll();
            Console.WriteLine("Users List:");
            foreach (var user in users)
            {
                Console.WriteLine(user.Id + "-" + user.FirstName + " " + user.LastName);
            }
        }

        public override async Task Update()
        {
            int id = GetNumber("Input user id: ");

            var user = (await _service.GetAll()).FirstOrDefault(p => p.Id == id);
            if (user == null)
            {
                Console.WriteLine("No user with this id");
                return;
            }

            user.BirthDay = GetDate($"Enter new user BirthDay date (old: {user.BirthDay}): ");
            user.FirstName = GetString($"Enter new user first name (old: {user.FirstName}): ");
            user.LastName = GetString($"Enter new user last name (old: {user.LastName}): ");
            user.Email = GetString($"Enter new user email (old: {user.Email}): ");
            user.TeamId = GetNumber($"Enter new Team Id for user (old: {user.TeamId}): ");

            if (await _service.Update(user))
                Console.WriteLine("Success with update user!");
            else
                Console.WriteLine("Error updating user!");
        }
    }
}
