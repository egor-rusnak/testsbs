﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.Application.Commands
{
    public class ExecuteFourthQuery : ICommand
    {
        private readonly IProjectService _projectService;
        public ExecuteFourthQuery(IProjectService service)
        {
            _projectService = service;
        }
        public string Description { get; set; } = "Execute query number 4";
        public async Task Execute()
        {
            Console.WriteLine("Teams with users older than 10 years: ");
            var result = await _projectService.ExecuteQueryFour();
            foreach (var elem in result)
            {
                Console.WriteLine($"Team [{elem.Key.Key} - {elem.Key.Value}] (ordered DESC by register date):");
                foreach (var user in elem.Value)
                {
                    Console.WriteLine("\t" + user + " Registered:  " + user.RegisteredAt + " BirthDay: " + user.BirthDay);
                }
                if (elem.Value.Count() == 0) Console.WriteLine("\tBLANK");

                Console.WriteLine("===============");
            }
            if (result.Count() == 0) Console.WriteLine("BLANK");
        }
    }
}
