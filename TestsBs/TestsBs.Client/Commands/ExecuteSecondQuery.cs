﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.Application.Commands
{
    public class ExecuteSecondQuery : ICommand
    {
        private readonly IProjectService _projectService;
        public ExecuteSecondQuery(IProjectService service)
        {
            _projectService = service;
        }
        public string Description { get; set; } = "Execute query number 2";
        public async Task Execute()
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Tasks for user [{input}] where task name length < 45: ");
                var result = await _projectService.ExecuteQueryTwo(input);
                foreach (var elem in result)
                {
                    Console.WriteLine($"{elem}");
                }
                if (result.Count() == 0) Console.WriteLine("BLANK");
            }

        }
    }
}
