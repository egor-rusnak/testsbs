﻿using System.Threading.Tasks;

namespace TestsBs.Application.Commands
{
    public interface ICommand
    {
        string Description { get; set; }
        Task Execute();
    }
}
