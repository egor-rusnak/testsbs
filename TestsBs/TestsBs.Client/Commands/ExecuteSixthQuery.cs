﻿using System;
using System.Threading.Tasks;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.Application.Commands
{
    public class ExecuteSixthQuery : ICommand
    {
        private readonly IProjectService _projectService;
        public ExecuteSixthQuery(IProjectService service)
        {
            _projectService = service;
        }

        public string Description { get; set; } = "Execute query number 6";
        public async Task Execute()
        {
            Console.Write("Input a user Id: ");
            if (int.TryParse(Console.ReadLine(), out int input))
            {
                Console.WriteLine($"Result for user {input}: ");
                var result = await _projectService.ExecuteQuerySix(input);
                if (result != null)
                {
                    Console.WriteLine($"[{result.User}]");
                    Console.WriteLine($"Last Project by Created date: {result.LastProject.Id} {result.LastProject.Name}");
                    Console.WriteLine($"Count of tasks in last project: {result.LastProjectTasksCount}");
                    Console.WriteLine($"Unfinished tasks: {result.UnfinishedTasksCount}");
                    Console.WriteLine("Longest task (if finished is null than using now): " + (result.LongestTask?.ToString() ?? "No tasks!"));
                }
                else Console.WriteLine("BLANK(No user in Data)");
            }
        }
    }
}
