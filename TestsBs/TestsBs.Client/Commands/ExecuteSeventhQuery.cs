﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TestsBs.UI.Model.Interfaces;

namespace TestsBs.Application.Commands
{
    public class ExecuteSeventhQuery : ICommand
    {
        private readonly IProjectService _projectService;
        public ExecuteSeventhQuery(IProjectService service)
        {
            _projectService = service;
        }
        public string Description { get; set; } = "Execute query number 7";
        public async Task Execute()
        {
            Console.WriteLine($"Result: ");
            var result = await _projectService.ExecuteQuerySeven();
            foreach (var elem in result)
            {
                Console.WriteLine($"Project [{elem.Project.Id} - {elem.Project.Name}]");
                Console.WriteLine($"Longest Task by date: [{elem.LongestTask}]");
                Console.WriteLine($"Shortest Task by name: [{elem.ShortestTask}]");
                Console.WriteLine($"Users count with project description > 20 length or tasks count <3: {elem.UsersCountInTeam?.ToString() ?? "Not match conditions"}");
                Console.WriteLine("=========================");
            }
            if (result.Count() == 0) Console.WriteLine("BLANK");
        }
    }
}
